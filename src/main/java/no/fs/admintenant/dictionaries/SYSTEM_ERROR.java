package no.fs.admintenant.dictionaries;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by denz0x13 on 01.12.15.
 */
public enum SYSTEM_ERROR {
    UNKNOWN_ERROR(5000,"General system error"),
    NO_IMPLEMENTED(5500,"Method not implemented"),
    OBJECT_NOT_FOUND(5404,"Object not found"),
    NO_OBJECT_DATA(5501,"Object data missing"),
    INVALID_OBJECT_DATA(5502,"Object data missing or invalid"),
    DUPLICATE_OBJECT_DATA(5503,"Object data missing"),
    DATA_NOT_SUPPORTED(5504,"Object data not supported"),
    AUTHENTICATION(4401,"Authentication error"),
    JWT_TOKEN_EXPIRED(4402,"Token expired"),
    NOT_PERMITTED(4301,"Not permitted")

    ;

    private final int code;
    private final String message;

    SYSTEM_ERROR(int code, String s) {
        this.code = code;
        this.message = s;
    }

    @JsonProperty
    public int getCode() {
        return code;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }


    @Override
    @JsonIgnore
    public String toString() {
        return String.format("[%d] : %s",code,message);
    }
}
