package no.fs.admintenant.domain.repositories;

import no.fs.admintenant.domain.model.base.BaseEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface IRepository<E extends BaseEntity> extends MongoRepository<E ,String> {
    void deleteByIdIn(List<String> ids);
}
