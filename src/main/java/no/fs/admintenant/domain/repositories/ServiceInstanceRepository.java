package no.fs.admintenant.domain.repositories;

import no.fs.admintenant.domain.model.ServiceInstance;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceInstanceRepository extends IRepository<ServiceInstance>{
    Long countByServiceSSLCertContentName(String name);
}
