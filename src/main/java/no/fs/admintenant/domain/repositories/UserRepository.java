package no.fs.admintenant.domain.repositories;

import no.fs.admintenant.domain.model.User;
import org.springframework.stereotype.Repository;

/**
 * Created by denz0x13 on 16.10.15.
 */
@Repository
public interface UserRepository extends IRepository<User> {
    User findByLoginNameAndPassword(String username, String Password);
    User findByEmailAndPassword(String username, String Password);
    User findByPasswordResetToken(String passwordResetToken);
    User findByLoginName(String loginName);
    User findByEmail(String email);
}
