package no.fs.admintenant.domain.repositories;

import no.fs.admintenant.domain.model.ServiceTemplate;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceTemplateRepository extends IRepository<ServiceTemplate>{
}
