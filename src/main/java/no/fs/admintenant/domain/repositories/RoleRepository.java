package no.fs.admintenant.domain.repositories;

import no.fs.admintenant.domain.model.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by denz0x13 on 15.12.15.
 */
@Repository
public interface RoleRepository extends IRepository<Role> {
    Role findFirstByName(String name);
    Long countAllByIdIn(List<Long> ids);
}
