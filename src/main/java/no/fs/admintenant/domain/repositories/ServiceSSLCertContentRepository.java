package no.fs.admintenant.domain.repositories;

import no.fs.admintenant.domain.model.ServiceSSLCertContent;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceSSLCertContentRepository extends IRepository<ServiceSSLCertContent>{
    ServiceSSLCertContent findFirstByName(String name);
}
