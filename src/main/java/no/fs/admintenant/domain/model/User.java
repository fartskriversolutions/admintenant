package no.fs.admintenant.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import no.fs.admintenant.domain.model.base.AuthEntity;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by denz0x13 on 15.12.15.
 */

@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
public class User extends AuthEntity<Role> implements Cloneable {

    @DBRef
    private List<Role> roles = new LinkedList<>();
    private Boolean enabled = true;
    private Boolean accountExpired = false;
    private Boolean accountLocked = false;
    private String timeZoneInfo;
    private TimeZone timeZone;


    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    @JsonIgnore
    public List<Role> getAuthRoles() {
        return getRoles();
    }

    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void addRole(Role role){
        this.roles.add(role);
    }

    public void removeRole(Role role) {
        for (Iterator<Role> iter = this.roles.iterator(); iter.hasNext(); )
        {
            if (iter.next().equals(role))
                iter.remove();
        }
    }


    @Override
    public String toString() {
        try {
            return String.format("%s %s [%s]",getFirstName(),getLastName(),getLoginName());
        }catch (Exception e){

        }
        return super.toString();
    }


    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for(Role role : getRoles()) {
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getName());
            authorities.add(authority);
        }
        return authorities;
    }

    @Override
    public String getUsername() {
        return getLoginName();
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return !getAccountExpired();
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return !getAccountLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonProperty("enabled")
    public boolean isEnabled() {
        return true;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getAccountExpired() {
        return accountExpired;
    }

    public void setAccountExpired(Boolean accountExpired) {
        this.accountExpired = accountExpired;
    }

    public Boolean getAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(Boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    @JsonIgnore
    public TimeZone getTimeZone() {
        if(timeZone==null){
            timeZone = TimeZone.getTimeZone(getTimeZoneInfo());
        }
        return timeZone;
    }

    public String getTimeZoneInfo() {
        if(StringUtils.isEmpty(timeZoneInfo)){
            timeZoneInfo = "UTC";
        }
        return timeZoneInfo;
    }

    public void setTimeZoneInfo(String timeZoneInfo) {
        this.timeZoneInfo = timeZoneInfo;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        User user = new User();

        user.firstName = firstName;
        user.lastName = lastName;
        user.enabled = true;
        user.setPassword(getPassword());
        user.setLoginName(getLoginName());
        user.setEmail(getEmail());
        user.accountExpired = getAccountExpired();
        user.accountLocked = getAccountLocked();
        user.timeZone = timeZone;


        return user;
    }
}
