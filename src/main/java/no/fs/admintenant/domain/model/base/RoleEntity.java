package no.fs.admintenant.domain.model.base;

/**
 * Created by denz0x13 on 15.12.15.
 */
public abstract class RoleEntity extends UserAuditEntity {
    private String name;
    private String description;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
