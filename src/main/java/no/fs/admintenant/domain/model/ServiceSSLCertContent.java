package no.fs.admintenant.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import no.fs.admintenant.domain.model.base.UserAuditEntity;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;

@Document
@Getter
@Setter
@EqualsAndHashCode
public class ServiceSSLCertContent extends UserAuditEntity{
    @NotEmpty
    @Indexed(unique = true)
    private String  name;
    @NotEmpty
    private String keyContent;
    @NotEmpty
    private String certContent;

    @NotNull
    private LocalDate expirationDate;

    @Override
    public String toString() {
        return name;
    }
}
