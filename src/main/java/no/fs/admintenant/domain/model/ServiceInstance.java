package no.fs.admintenant.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import no.fs.admintenant.domain.model.base.UserAuditEntity;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;

@Document
@Getter
@Setter
@EqualsAndHashCode
public class ServiceInstance extends UserAuditEntity{
    @NotEmpty
    @Indexed(unique = true)
    private String name;
    @NotEmpty
    @Indexed(unique = true)
    private String domainName;
    @DBRef
    private ServiceSSLCertContent serviceSSLCertContent;
    @DBRef
    @NotNull(message = "No service template")
    private ServiceTemplate serviceTemplate;

    private Boolean deployed = false;

    public String getDockerServiceName(){
        if(domainName==null){
            return null;
        }
        return this.getName().replace(".","_");
    }

}
