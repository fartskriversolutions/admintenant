package no.fs.admintenant.domain.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode
public abstract class BaseEntity implements Serializable {
    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonIgnore
    public JsonNode toJson(){
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.valueToTree(this);
    }





}
