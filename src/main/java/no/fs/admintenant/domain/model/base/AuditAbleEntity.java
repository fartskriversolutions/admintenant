package no.fs.admintenant.domain.model.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import java.util.Date;

/**
 * Created by denz0x13 on 30.10.15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AuditAbleEntity extends BaseEntity {
    protected Date createdAt;
    protected Date updatedAt;


    @CreatedDate
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


    @LastModifiedDate
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


}
