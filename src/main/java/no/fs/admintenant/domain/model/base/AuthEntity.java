package no.fs.admintenant.domain.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * Created by denz0x13 on 15.12.15.
 */
public abstract class AuthEntity<T extends RoleEntity> extends UserAuditEntity implements UserDetails {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String loginName;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }



    private String password;

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public abstract List<T> getAuthRoles();


    public String toString(){
        return String.format("%s",getLoginName());
    }

    private String passwordResetToken;

    @JsonIgnore
    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }
}
