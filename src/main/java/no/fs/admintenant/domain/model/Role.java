package no.fs.admintenant.domain.model;


import lombok.EqualsAndHashCode;
import no.fs.admintenant.domain.model.base.RoleEntity;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * Created by denz0x13 on 15.12.15.
 */
@Document
@EqualsAndHashCode
public class Role extends RoleEntity {


    @Override
    public String toString() {
        return this.getName();
    }



}
