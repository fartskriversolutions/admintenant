package no.fs.admintenant.domain.model.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import no.fs.admintenant.domain.model.User;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.persistence.*;

/**
 * Created by denz0x13 on 24.12.15.
 */
public abstract class UserAuditEntity extends AuditAbleEntity {

    @DBRef
    protected User createdBy;
    @DBRef
    protected User updatedBy;

    @JsonIgnore
    @CreatedBy
    public User getCreatedBy(){
        return this.createdBy;
    }

    public void setCreatedBy(User user){
        this.createdBy = user;
    }

    @JsonIgnore
    @LastModifiedBy
    public User getUpdatedBy(){
        return this.updatedBy;
    }

    public void setUpdatedBy(User user){
        this.updatedBy = user;
    }
}
