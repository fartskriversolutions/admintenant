package no.fs.admintenant.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import no.fs.admintenant.domain.model.base.UserAuditEntity;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
@EqualsAndHashCode
public class ServiceTemplate extends UserAuditEntity{
    @NotEmpty
    @Indexed(unique = true)
    private String name;
    private String description;
    private Long virtualPort=8080L;
    private String virtualHost="";
    @NotEmpty
    private String composeTemplate;

    @Override
    public String toString() {
        return name;
    }
}
