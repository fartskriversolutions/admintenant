package no.fs.admintenant.domain.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import no.fs.admintenant.config.aaa.Authorities;
import no.fs.admintenant.datatypes.CommonParams;
import no.fs.admintenant.dictionaries.SYSTEM_ERROR;
import no.fs.admintenant.domain.model.Role;
import no.fs.admintenant.domain.model.base.AuditAbleEntity;
import no.fs.admintenant.domain.model.base.AuthEntity;
import no.fs.admintenant.domain.model.base.BaseEntity;
import no.fs.admintenant.domain.repositories.IRepository;
import no.fs.admintenant.domain.services.exeptions.ServiceException;
import no.fs.admintenant.helpers.SpringHelpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public abstract class BaseService <T extends BaseEntity, R extends IRepository<T>> {

    /**
     * The constant DEF_LIMIT.
     */
    public static final Integer DEF_LIMIT = 10000;
    /**
     * The constant DEF_OFFSET.
     */
    public static final Integer DEF_OFFSET = 0;

    protected final TypeToken<T> typeToken = new TypeToken<T>(getClass()) {
    };
    private final Type type = typeToken.getType();
    /**
     * The Logger.
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * The User management service.
     */
    @Autowired
    UserManagementService userManagementService;

    /**
     * Gets repository.
     *
     * @return the repository
     */
    public abstract R getRepository();

    public abstract T newEntity();

    /**
     * The constant objectMapper.
     */
    public static ObjectMapper objectMapper = new ObjectMapper();

    protected String[] getMergeIgnoringProperties() {
        return new String[]{"id"};
    }

    static {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public String getModelName() {
        return type.getTypeName();
    }

    /**
     * Gets model class.
     *
     * @return the model class
     */
    public Class<T> getModelClass() {
        return (Class<T>) typeToken.getRawType();
    }

    public Long getTimeNow() {
        return Instant.now().toEpochMilli();
    }

    public AuthEntity getCurrentUser() {
        try {
            return ((AuthEntity) (SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        } catch (Exception e) {

        }
        return null;
    }

    public Boolean hasRole(String... roles) {
        AuthEntity user = getCurrentUser();
        return hasRole(user, roles);
    }

    public Boolean hasRole(AuthEntity user, String... roles) {
        if (user == null) {
            return false;
        }
        final List _roles = Arrays.asList(roles);
        Optional<? extends GrantedAuthority> _f = user.getAuthorities().
                stream().filter(grantedAuthority -> _roles.contains(grantedAuthority.getAuthority())).findAny();

        return _f.isPresent();
    }

    /**
     * Gets roles of current user.
     *
     * @return the roles
     */
    public List<Role> getRoles() {
        AuthEntity user = getCurrentUser();
        if (user == null) {
            return new ArrayList<>();
        }
        return user.getAuthRoles();
    }

    public Boolean isAdmin() {
        return hasRole(Authorities.ADMIN.getRoleName());
    }

    public Boolean isAdmin(AuthEntity user) {
        return hasRole(user, Authorities.ADMIN.getRoleName());
    }

    /**
     * Save t.
     *
     * @param entity the entity
     * @return the t
     */
    public T save(T entity) {
        return getRepository().save(entity);
    }

    /**
     * Save iterable.
     *
     * @param entities the entities
     * @return the iterable
     */
    public Iterable<T> save(List<T> entities) {
        return getRepository().save(entities);
    }

    public void delete(String id) {
        if (canDelete(id)) {
            //deletePermissions(id);
            getRepository().delete(id);
        }
    }



    /**
     * Delete.
     *
     * @param ids the ids
     */
    public void delete(List<String> ids) {
        List<String> _permitted_ids = canDelete(ids);
        if(_permitted_ids.size() < 1){
            return;
        }
        //deletePermissions(_permitted_ids);
        getRepository().deleteByIdIn(_permitted_ids);
        //getRepository().delete(getRepository().findAll(_permitted_ids));
        OnDeletePermitted(_permitted_ids);
    }



    public Boolean canDeleteHook(String id){
        return true;
    }

    public void OnDeletePermitted(List<String> ids){

    }

    public T create(T entity) throws ServiceException {
        T _r = save(merge(newEntity(), entity));
        //grantPermission(_r, BasePermission.ADMINISTRATION);
        return _r;
    }

    public T update(String id, T entity) throws ServiceException {
        T t = findOne(id);
        t = merge(t, entity);
        return save(t);
    }

    public T createOrUpdate(T entity) throws ServiceException {
        if (entity.getId() != null) {
            return update(entity.getId(), entity);
        }
        return create(entity);
    }

    /**
     * Merge t.
     *
     * @param oldEntity the old entity
     * @param newEntity the new entity
     * @return the t
     * @throws ServiceException the service exception
     */
    public T merge(T oldEntity, T newEntity) throws ServiceException {
        try {
            SpringHelpers.copyProperties(newEntity, oldEntity, false, getMergeIgnoringProperties());
        } catch (Exception e) {
            throw new ServiceException(e);
        }
        return oldEntity;
    }


    public T findOne(String id) throws ServiceException {
        T _result = getRepository().findOne(id);
        if (_result == null) {
            throw new ServiceException(SYSTEM_ERROR.OBJECT_NOT_FOUND, id, getModelName());
        }
        return _result;
    }

    public Page<T> findAll(Map<String,Object> params){
        return findAll(params,getPage(params));
    }

    public Page<T> findAll(Map<String, Object> params, Pageable pageable){
        return getRepository().findAll(pageable);
    }

    Pageable getPage(Map<String, Object> params) {
        Integer limit = DEF_LIMIT;
        Integer offset = DEF_OFFSET;
        if (params.containsKey(CommonParams.CONST_LIMIT)) {
            limit = (Integer) params.get(CommonParams.CONST_LIMIT);
        }
        if (params.containsKey(CommonParams.CONST_OFFSET)) {
            offset = (Integer) params.get(CommonParams.CONST_OFFSET);
        }
        Sort sort = getSort(params);
        if (sort == null) {
            sort = getSort();
        }
        if (sort != null) {
            return new PageRequest(offset, limit, sort);
        }
        Pageable page = new PageRequest(offset, limit);
        return page;
    }

    public Long count(Map<String, Object> params) {
        return getRepository().count();
    }

    public Long count() {
        return getRepository().count();
    }



    /**
     * Gets sort if getSort with param returned null;
     *
     * @return the sort
     */
    public Sort getSort() {
        try {
            if (typeToken.isSubtypeOf(AuditAbleEntity.class)) {
                //Field _f = getModelClass().getField("createdAt");
                Sort sort = new Sort(new Sort.Order(Sort.Direction.DESC, "createdAt"));
                return sort;
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return null;
    }

    /**
     * Create sort from params
     *
     * @param params
     * @return
     */
    public Sort getSort(Map<String, Object> params) {
        List<Sort.Order> _orders = extractOrder(params);
        if (_orders.isEmpty()) {
            return null;
        }
        List<Sort.Order> _validOrders = new CopyOnWriteArrayList<>();
        for (Sort.Order _order : _orders) {
            Set<Class<? super T>> _set = typeToken.getTypes().rawTypes();
            for (Class _clazz : _set) {
                try {
                    _clazz.getDeclaredField(_order.getProperty());
                } catch (NoSuchFieldException e) {
                    continue;
                }
                _validOrders.add(_order);
                break;
            }

        }
        if (_validOrders.isEmpty()) {
            return null;
        }
        return new Sort(_validOrders);
    }

    public List<Sort.Order> extractOrder(Map<String, Object> params) {
        List result = new CopyOnWriteArrayList<Sort.Order>();
        if (!params.containsKey(CommonParams.CONST_SORT)) {
            return result;
        }
        if (!(params.get(CommonParams.CONST_SORT) instanceof Map)) {
            return result;
        }
        Map<String, Object> _sort = (Map) params.get(CommonParams.CONST_SORT);
        for (String _key : _sort.keySet()) {
            Sort.Direction direction = Sort.Direction.fromStringOrNull(_key);
            if (direction == null) {
                continue;
            }
            if (_sort.get(_key) == null) {
                continue;
            }
            if (_sort.get(_key) instanceof List) {
                for (Object _property : ((List) _sort.get(_key))) {
                    result.add(new Sort.Order(direction, _property.toString()));
                }
            } else {
                result.add(new Sort.Order(direction, _sort.get(_key).toString()));
            }
        }
        return result;
    }




    public boolean canUpdate(String id) {
        return true;
    }

    public boolean canEdit(String id) {
        return true;
    }

    public boolean canCreate() {
        return true;
    }

    public Boolean canDelete(String id){
        return true;
    }

    public List<String> canDelete(List<String> ids) {
        return canDeleteEach(ids);
    }

    public List<String> canDeleteEach(List<String> ids) {
        return ids.stream().filter(id -> canDeleteHook(id)).collect(Collectors.toList());
    }

}
