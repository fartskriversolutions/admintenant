package no.fs.admintenant.domain.services;

import no.fs.admintenant.domain.model.ServiceInstance;
import no.fs.admintenant.domain.model.ServiceSSLCertContent;
import no.fs.admintenant.domain.repositories.ServiceInstanceRepository;
import no.fs.admintenant.domain.repositories.ServiceSSLCertContentRepository;
import no.fs.admintenant.domain.services.exeptions.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ServiceSSLCertContentService extends BaseService<ServiceSSLCertContent,ServiceSSLCertContentRepository>{

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    ServiceSSLCertContentRepository serviceSSLCertContentRepository;

    @Autowired
    ServiceInstanceRepository serviceInstanceRepository;

    @Override
    public ServiceSSLCertContentRepository getRepository() {
        return serviceSSLCertContentRepository;
    }

    @Override
    public ServiceSSLCertContent newEntity() {
        return new ServiceSSLCertContent();
    }

    public boolean inUse(String certName){
        ServiceSSLCertContent cert = getRepository().findFirstByName(certName);
        if(cert==null){
            return false;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("serviceSSLCertContent").is(cert));
        List<ServiceInstance> _insts = mongoTemplate.find(query,ServiceInstance.class);
        return _insts.size() > 0;
    }

    @Override
    public Boolean canDelete(String id) {
        try {
            ServiceSSLCertContent cert = findOne(id);
            if(inUse(cert.getName())){
                return false;
            }
        } catch (ServiceException e) {

        }
        return super.canDelete(id);
    }
}
