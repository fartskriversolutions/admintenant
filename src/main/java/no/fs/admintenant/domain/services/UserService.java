package no.fs.admintenant.domain.services;

import no.fs.admintenant.domain.model.User;
import no.fs.admintenant.domain.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UserService extends BaseService<User, UserRepository>{

    @Autowired
    UserRepository userRepository;

    private static Pattern BCRYPT_PATTERN = Pattern
            .compile("\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}");

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }

    @Override
    public User newEntity() {
        return new User();
    }

    public User encodePassword(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        return entity;
    }
}
