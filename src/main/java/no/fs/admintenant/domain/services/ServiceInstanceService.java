package no.fs.admintenant.domain.services;

import no.fs.admintenant.domain.model.ServiceInstance;
import no.fs.admintenant.domain.model.ServiceTemplate;
import no.fs.admintenant.domain.repositories.ServiceInstanceRepository;
import no.fs.admintenant.domain.services.exeptions.ServiceException;
import no.fs.admintenant.helpers.SpringHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceInstanceService extends BaseService<ServiceInstance,ServiceInstanceRepository>{

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    ServiceInstanceRepository serviceInstanceRepository;

    @Override
    public ServiceInstanceRepository getRepository() {
        return serviceInstanceRepository;
    }

    @Override
    public ServiceInstance newEntity() {
        return new ServiceInstance();
    }

    public List<ServiceInstance> getByTemplate(ServiceTemplate serviceTemplate){
        Query query = new Query();
        query.addCriteria(Criteria.where("serviceTemplate").is(serviceTemplate));
        List<ServiceInstance> _insts = mongoTemplate.find(query,ServiceInstance.class);
        return _insts;
    }

    public List<ServiceInstance> getDeployedByTemplate(ServiceTemplate serviceTemplate){
        Query query = new Query();
        query.addCriteria(Criteria.where("serviceTemplate").is(serviceTemplate).andOperator(Criteria.where("deployed").is(true)));
        List<ServiceInstance> _insts = mongoTemplate.find(query,ServiceInstance.class);
        return _insts;
    }

    @Override
    public boolean canEdit(String id) {
        return super.canEdit(id);
    }

    @Override
    public boolean canUpdate(String id) {

        return super.canUpdate(id);
    }

    @Override
    public Boolean canDelete(String id) {
        try {
            ServiceInstance serviceInstance = findOne(id);
            if(serviceInstance.getDeployed()){
                return false;
            }

        } catch (ServiceException e) {
            return false;
        }
        return super.canDelete(id);
    }

    @Override
    public ServiceInstance update(String id, ServiceInstance entity) throws ServiceException {
        return super.update(id, entity);
    }

    @Override
    public ServiceInstance merge(ServiceInstance oldEntity, ServiceInstance newEntity) throws ServiceException {
        String[] _ignored = getMergeIgnoringProperties();
        if(oldEntity.getId()!=null && oldEntity.getDeployed()){
            _ignored = new String[]{"id","name","deployed","serviceTemplate"};
        }
        try {
            SpringHelpers.copyProperties(newEntity, oldEntity, false, _ignored);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
        return oldEntity;
    }
}
