package no.fs.admintenant.domain.services;

import no.fs.admintenant.domain.model.ServiceTemplate;
import no.fs.admintenant.domain.repositories.ServiceTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceTemplateService extends BaseService<ServiceTemplate,ServiceTemplateRepository>{

    @Autowired
    ServiceTemplateRepository serviceTemplateRepository;

    @Override
    public ServiceTemplateRepository getRepository() {
        return serviceTemplateRepository;
    }

    @Override
    public ServiceTemplate newEntity() {
        return new ServiceTemplate();
    }
}
