package no.fs.admintenant.domain.services;

import no.fs.admintenant.config.aaa.Authorities;
import no.fs.admintenant.domain.model.Role;
import no.fs.admintenant.domain.model.User;
import no.fs.admintenant.domain.repositories.RoleRepository;
import no.fs.admintenant.domain.repositories.UserRepository;
import no.fs.admintenant.domain.services.exeptions.ServiceException;
import no.fs.admintenant.helpers.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denz0x13 on 16.10.15.
 */
@Service(value = "userManagementService")
@EnableAsync(proxyTargetClass = true)
@EnableCaching(proxyTargetClass = true)
public class UserManagementService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;



    public User getCurrentUser() {
        try {
            return ((User) (SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        } catch (Exception e) {

        }

        return null;
    }

    public Boolean hasRole(User user,String role){
        return userService.hasRole(user, role);
    }

    public User getByPasswordAndUserName(String username, String password) {
        User user = getByLoginName(username);
        if(user == null){
            user = userRepository.findByEmail(username);
        }
        if (user == null) {
            return null;
        }
        if (!passwordEncoder.matches(password, user.getPassword())) {
            return null;
        }
        return user;
    }

    public User getByLoginName(String loginName) {
        //checkUsers();
        return userRepository.findByLoginName(loginName);
    }

    public User getByUsername(String userName) {
        return getByLoginName(userName);
    }


    public List<GrantedAuthority> getGrantedAuthority(User user) {
        return new ArrayList<>(user.getAuthorities());
    }


    public void checkUsers() {

        getSystemService();

        if (userRepository.count() <= 1 && userRepository.findByLoginName("admin_super") == null) {
            User user = new User();
            user.setLoginName("admin_super");
            user.setFirstName("Admin");
            user.setLastName("Super");
            user.setPassword("12345678");
            user.setEmail("admin@example.com");
            Role role = getRole(Authorities.ADMIN.getRoleName());
            user.addRole(role);
            user = userService.encodePassword(user);
            try {
                user = userService.create(user);
               // userService.grantToUser(user, user, BasePermission.ADMINISTRATION);
            } catch (ServiceException e) {
                e.printStackTrace();
            }

        }


    }

    public final static String SYSTEM_SERVICE_USERNAME = "system_service";

    public User getSystemService() {
        User sa = userRepository.findByLoginName(SYSTEM_SERVICE_USERNAME);
        if (sa == null) {
            User user = new User();
            user.setLoginName(SYSTEM_SERVICE_USERNAME);
            user.setFirstName("System");
            user.setLastName("Service");
            user.setPassword(StringHelper.randomString(64));
            user.setEmail("system.service@cloudconsulting.no");
            Role role = getAdminRole();
            user.addRole(role);
            try {
                user = userService.create(userService.encodePassword(user));
            } catch (ServiceException e) {
                e.printStackTrace();
            }

        }
        return sa;
    }

    public User loginSA() {
        SecurityContext sc = SecurityContextHolder.createEmptyContext();
        User sa = getSystemService();
        sc.setAuthentication(new UsernamePasswordAuthenticationToken(sa, null, sa.getAuthorities()));
        SecurityContextHolder.setContext(sc);
        return sa;
    }

    public User loginUser(User user){
        SecurityContext sc = SecurityContextHolder.createEmptyContext();
        sc.setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));
        SecurityContextHolder.setContext(sc);
        return user;
    }

    public User loginUser(String username, String password) throws AuthenticationException {
        User user = getByPasswordAndUserName(username,password);
        if(user == null){
            throw new AuthenticationServiceException("User not found");
        }
        return loginUser(user);
    }

    public User loginUser(String username, String password, String roleName) throws AuthenticationException {
        User user = getByPasswordAndUserName(username, password);
        if (user == null) {
            throw new AuthenticationServiceException("User not found");
        }
        if (!hasRole(user, roleName)) {
            throw new AuthenticationServiceException("Not allowed by role");
        }
        return loginUser(user);
    }

    public void logoutUser(){
        SecurityContextHolder.clearContext();
    }


//    public static PrincipalSid getSASid() {
//        return new PrincipalSid(SYSTEM_SERVICE_USERNAME);
//    }


    public Role getRole(String roleName) {
        Role role = roleRepository.findFirstByName(roleName);
        if (role == null) {
            role = new Role();
            role.setName(roleName);
            role = roleRepository.save(role);
        }
        return role;
    }

    public Role getAdminRole() {
        return getRole(Authorities.ADMIN.getRoleName());
    }

    public Role getUserRole() {
        return getRole(Authorities.USER.getRoleName());
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails ud = (UserDetails) getByUsername(username);
        return ud;
    }



    public String generateNewPassword(User user){
        String password = StringHelper.randomString(10);
        user.setPasswordResetToken("");
        user.setPassword(passwordEncoder.encode(password));
        user = userRepository.save(user);

        return password;
    }




}
