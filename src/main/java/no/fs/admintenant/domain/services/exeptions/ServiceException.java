package no.fs.admintenant.domain.services.exeptions;


import no.fs.admintenant.dictionaries.SYSTEM_ERROR;

/**
 * Created by denz0x13 on 18.12.15.
 */
public class ServiceException extends Exception {
    private SYSTEM_ERROR errorCode;
    Throwable throwable;
    public ServiceException(SYSTEM_ERROR system_error){
        super(system_error.getMessage());
        this.errorCode =system_error;
    }

    public ServiceException(SYSTEM_ERROR system_error, Class klass){
        super(String.format("%s, Class: %s",system_error.getMessage(),klass.getTypeName()));
        this.errorCode =system_error;
    }

    public ServiceException(SYSTEM_ERROR system_error, String objectId, Class klass){
        super(String.format("%s. Object ID: %s, Class: %s",system_error.getMessage(),objectId,klass.getTypeName()));
        this.errorCode = system_error;
    }

    public ServiceException(SYSTEM_ERROR system_error, String objectId, String klazzName){
        super(String.format("%s. Object ID: %s, Class: %s",system_error.getMessage(),objectId,klazzName));
        this.errorCode = system_error;
    }

    public ServiceException(Throwable e){
        super(e);
        this.throwable=e;
        this.errorCode = SYSTEM_ERROR.UNKNOWN_ERROR;
    }

    public Integer getCode(){
        return errorCode.getCode();
    }

//    public ResponseEntity<ApiResponse> getResponse(){
//        return new ResponseEntity<ApiResponse>(
//                ApiResponse.builder().errors(errorCode.getCode(),errorCode.getMessage()).build(),
//                HttpStatus.INTERNAL_SERVER_ERROR
//        );
//    }

    public String getMessage(){
        if(throwable!=null){
            return throwable.getMessage();
        }
        return super.getMessage();
    }
}
