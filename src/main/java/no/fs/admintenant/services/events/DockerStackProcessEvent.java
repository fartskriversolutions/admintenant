package no.fs.admintenant.services.events;

import lombok.Getter;
import lombok.Setter;

import java.io.InputStream;


@Getter
@Setter
public class DockerStackProcessEvent {
    public enum OUTPUT_TYPE {
        OUTPUT,
        ERROR
    }
    private InputStream is;
    private OUTPUT_TYPE type;

    public DockerStackProcessEvent(InputStream is, OUTPUT_TYPE type) {
        this.is = is;
        this.type = type;
    }


}
