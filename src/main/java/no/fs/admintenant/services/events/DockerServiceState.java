package no.fs.admintenant.services.events;


public enum DockerServiceState {
    DOCKER_ENGINE,
    PROXY_NETWORK,
    CERT_VOLUME
}
