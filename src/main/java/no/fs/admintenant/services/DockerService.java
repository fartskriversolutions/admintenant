package no.fs.admintenant.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.InspectVolumeResponse;
import com.github.dockerjava.api.command.ListVolumesResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.api.model.Network;
import no.fs.admintenant.domain.model.ServiceInstance;
import no.fs.admintenant.domain.model.ServiceSSLCertContent;
import no.fs.admintenant.domain.services.ServiceInstanceService;
import no.fs.admintenant.services.events.DockerStackProcessEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.spring.events.EventBus;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DockerService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final String PROXY_NETWORK = "proxy";
    public static final String PROXY_CERT_VOLUME = "proxy_cert_volume";

    private static String dockerCMD = "docker";

    @Autowired
    DockerClient dockerClient;

    @Autowired
    EventBus.ApplicationEventBus eventBus;

    @Autowired
    ServiceInstanceService serviceInstanceService;

    private Boolean dockerEngineIsPresent = false;
    private Info dockerInfo = null;
    private Network proxyNetwork = null;
    private InspectVolumeResponse proxyCertVolume;
    private Integer lastCmdExitCode = -1;
    private List<String> lastCmdOut=new ArrayList<>();

    @PostConstruct
    void init() {
        initVars();
        try {
            dockerInfo = dockerClient.infoCmd().exec();
            dockerEngineIsPresent = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            dockerEngineIsPresent = false;
        }

        if (!dockerEngineIsPresent) {
            return;
        }


        List<Network> network = dockerClient.listNetworksCmd().withNameFilter(PROXY_NETWORK).exec();
        if (network.size() > 0) {
            proxyNetwork = network.get(0);
        }

        ListVolumesResponse _result = dockerClient.listVolumesCmd().exec();
        List<InspectVolumeResponse> _volumes = _result.getVolumes();
        if (_volumes != null) {
            Optional<InspectVolumeResponse> _vol = _volumes.stream().filter(_v -> _v.getName().equals(PROXY_CERT_VOLUME)).findFirst();
            if (_vol.isPresent()) {
                proxyCertVolume = _vol.get();
            }
        }

    }

    String getDockerCMD(){
        return dockerCMD;
    }

    public DockerService reload() {
        init();
        return this;
    }

    private void initVars() {
        dockerEngineIsPresent = false;
        dockerInfo = null;
        proxyNetwork = null;
    }

    public DockerClient getDockerClient() {
        return dockerClient;
    }

    public Boolean getDockerEngineIsPresent() {
        return dockerEngineIsPresent;
    }

    public Boolean isProxyNetworkPresent() {
        return dockerEngineIsPresent && proxyNetwork != null;
    }

    public Info getDockerInfo() {
        return dockerInfo;
    }

    public Network getProxyNetwork() {
        return proxyNetwork;
    }

    public InspectVolumeResponse getProxyCertVolume() {
        return proxyCertVolume;
    }

    public Boolean isProxyCertVolumePresent() {
        return dockerEngineIsPresent && proxyCertVolume != null;
    }

    public synchronized void deployStack(ServiceInstance serviceInstance) {
        if (serviceInstance.getServiceTemplate() == null) {
            logger.error("Invalid template");
        }

        String tempDir = System.getProperty("java.io.tmpdir");
        String template_file_name = tempDir + File.separator + serviceInstance.getDomainName() + ".yml";
        Path template = Paths.get(template_file_name);
        try {
            Files.write(template, serviceInstance.getServiceTemplate().getComposeTemplate().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        String _cmd = String.format("%s stack deploy --with-registry-auth --compose-file %s %s",
                 getDockerCMD(), template_file_name,serviceInstance.getDockerServiceName());

        logger.info("Prepare exec: " + _cmd);

        Runtime runtime = Runtime.getRuntime();

        ServiceSSLCertContent serviceSSLCertContent = serviceInstance.getServiceSSLCertContent();

        String env[] = new String[]{
                String.format("VIRTUAL_HOST=%s", serviceInstance.getDomainName()),
                String.format("VIRTUAL_PORT=%d", serviceInstance.getServiceTemplate().getVirtualPort()),
                String.format("CERT_NAME=%s",serviceSSLCertContent!=null ? serviceSSLCertContent.getName(): "")
        };

        updateCertificates(serviceSSLCertContent,false);

        execExtDockerCmdWithOut(_cmd,env);
        serviceInstance.setDeployed(lastCmdExitCode==0);
        serviceInstanceService.save(serviceInstance);

        template.toFile().delete();
    }

    public synchronized void updateCertificates(ServiceSSLCertContent serviceSSLCertContent,boolean force){
        if(serviceSSLCertContent!=null){
            Path _key = Paths.get("certs/"+serviceSSLCertContent.getName()+".key");
            Path _crt = Paths.get("certs/"+serviceSSLCertContent.getName()+".crt");

            try {
                if(!Files.exists(_key) || force) {
                    Files.write(_key, serviceSSLCertContent.getKeyContent().getBytes());
                }
                if(!Files.exists(_crt) || force) {
                    Files.write(_crt, serviceSSLCertContent.getCertContent().getBytes());
                }
            } catch (IOException e) {
                logger.error(e.getMessage(),e);
            }
        }
    }

    public synchronized Integer stopStack(ServiceInstance serviceInstance){
        String _cmd = String.format("%s stack rm  %s",
                getDockerCMD(),serviceInstance.getDockerServiceName());

        Integer _extVal = execExtDockerCmd(_cmd);
        return _extVal;
    }

    public synchronized void  deleteStack(ServiceInstance serviceInstance){
        if(stopStack(serviceInstance)!=0){
            return;
        }

        while (getInternalContainers(serviceInstance).size() > 0){
            logger.info("Waiting for all containers are exited");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        getInternalVolumes(serviceInstance).stream().forEach(_v -> {
            logger.info("Removing volume name: "+_v.getName());
            dockerClient.removeVolumeCmd(_v.getName()).exec();
        });

        serviceInstance.setDeployed(false);
        serviceInstanceService.save(serviceInstance);

    }

    private Integer execExtDockerCmd(String _cmd){
        return execExtDockerCmd(_cmd,new String[]{});
    }

    private Integer execExtDockerCmd(String _cmd, String[] env){
        Integer exitVal = -1;
        Runtime runtime = Runtime.getRuntime();
        logger.info("Prepare exec: " + _cmd);
        try{
            Process process = runtime.exec(_cmd,env);
            eventBus.publish(this, new DockerStackProcessEvent(process.getInputStream(), DockerStackProcessEvent.OUTPUT_TYPE.OUTPUT));
            exitVal = process.waitFor();
            logger.info("Process finished with exit code: " + process.exitValue());
        }catch (Exception ex){
            logger.error(ex.getMessage(),ex);
        }

        return exitVal;
    }
    private List<String> execExtDockerCmdWithOut(String _cmd){
        return execExtDockerCmdWithOut(_cmd,new String[]{});
    }

    private List<String> execExtDockerCmdWithOut(String _cmd, String[] env){
        Runtime runtime = Runtime.getRuntime();
        lastCmdExitCode = -1;
        lastCmdOut = Collections.emptyList();
        logger.info("Prepare exec: " + _cmd);
        try{
            Process process = runtime.exec(_cmd,env);
            StreamGobbler sg = new StreamGobbler(process.getInputStream());
            sg.start();
            lastCmdExitCode = process.waitFor();
            while (sg.isRunning()){
                Thread.sleep(250);
            }
            logger.info("Process finished with exit code: " + process.exitValue());
            lastCmdOut = sg.getOutLines();
            return sg.getOutLines();
        }catch (Exception ex){
            logger.error(ex.getMessage(),ex);
        }

        return lastCmdOut;
    }

    private Map<String,Object> readServiceTemplate(ServiceInstance serviceInstance){
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try{
            ObjectNode objectNode = (ObjectNode)mapper.readTree(serviceInstance.getServiceTemplate().getComposeTemplate());
            Map<String, Object> map = (Map<String, Object>) mapper.convertValue(objectNode,Map.class);
            return map;
        }catch (Exception ex){
            logger.error(ex.getMessage(),ex);
        }
        return (Map<String, Object>) Collections.EMPTY_MAP;
    }

    private List<InspectVolumeResponse> getInternalVolumes(ServiceInstance serviceInstance){
        List<InspectVolumeResponse> _result = new ArrayList<>();
        ListVolumesResponse _resp = dockerClient.listVolumesCmd().exec();
        List<InspectVolumeResponse> _volumes = _resp.getVolumes();
        if (_volumes != null) {
             _result = _volumes.stream().filter(_v -> _v.getName().startsWith(serviceInstance.getDockerServiceName())).collect(Collectors.toList());
        }
        return _result;
    }

    private List getInternalContainers(ServiceInstance serviceInstance){
        List<Container> _containers = dockerClient.listContainersCmd().exec();
        List<Container> _result = new ArrayList<>();
        if(_containers!=null){
            for(Container _c: _containers){
                for(String _n : _c.getNames()){
                    if(_n.startsWith("/"+serviceInstance.getDockerServiceName())){
                        _result.add(_c);
                    }
                }
            };
        }
        return _result;
    }

    public List<String> getDeployedStackOnHost(){
        String _cmd = String.format("%s stack ls",getDockerCMD());
        List<String> _result = new ArrayList<>();
        execExtDockerCmd(_cmd);
        return _result;
    }

    public Integer getLastCmdExitCode() {
        return lastCmdExitCode;
    }

    public List<String> getLastCmdOut() {
        return lastCmdOut;
    }

    class StreamGobbler extends Thread {
        private InputStream is;
        private List<String> outLines;
        private Boolean isRunning;
        StreamGobbler(InputStream is){
            this.is = is;
            outLines=new ArrayList<>();
            isRunning = false;
        }

        @Override
        public void run() {
            isRunning = true;
            try
            {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line=null;
                while ( (line = br.readLine()) != null) {
                    logger.info(line);
                    outLines.add(line);
                }
            } catch (IOException ioe)
            {
                ioe.printStackTrace();
            }
            isRunning = false;
        }

        public Boolean isRunning(){
            return isRunning;
        }

        public List<String> getOutLines(){
            return outLines;
        }
    }
}
