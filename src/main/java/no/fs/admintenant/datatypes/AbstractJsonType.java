package no.fs.admintenant.datatypes;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by denz0x13 on 04.11.15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractJsonType implements Serializable,Cloneable {


    public static JsonNode fromResourceFile(String filePath) throws IOException {
        ClassPathResource classPathResource = new ClassPathResource(filePath);
        InputStream inputStream = classPathResource.getInputStream();
        return objectMapper.readTree(inputStream);
    }

    public static String JsonToString(JsonNode node) throws JsonProcessingException {
        return  objectMapper.writeValueAsString(node);
    }
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    protected static ObjectMapper objectMapper = new ObjectMapper();

    ObjectMapper getObjectMapper(){
        return objectMapper;
    }

    @JsonIgnore
    public String toJson(){
        String _resultJson = "";
        try {
            _resultJson =  objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return  _resultJson;
    }

    @JsonIgnore
    public JsonNode asJson(){
        return objectMapper.valueToTree(this);
    }

    @Override
    @JsonIgnore
    public AbstractJsonType clone() throws CloneNotSupportedException {
        try{
            return (AbstractJsonType) getObjectMapper().readValue(toJson(),this.getClass());
        }catch (IOException e) {
            e.printStackTrace();
            throw new CloneNotSupportedException(e.getMessage());
        }
    }

    @Override
    @JsonIgnore
    public String toString() {
        return toJson();
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AbstractJsonType withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @JsonIgnore
    public Object get(String key){
        return this.additionalProperties.get(key);
    }
}
