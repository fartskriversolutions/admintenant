package no.fs.admintenant.datatypes;

public class CommonParams {
    public static final long MONTH_SEC = 30 * 24 * 3600 * 1000L;
    public static final String CONST_LIMIT = "limit";
    public static final String CONST_DATE_START = "date_start";
    public static final String CONST_DATE_END = "date_end";
    public static final String CONST_DATE = "date";
    public static final String CONST_OFFSET = "offset";
    public static final String CONST_SEARCH_TEXT = "search_text";
    public static final String CONST_ZONE_OFFSET = "zoneOffset";
    public static final String CONST_IDS = "ids";
    public static final String CONST_TAGS = "tags";
    public static final String CONST_SORT = "sort";
    public static final String CONST_FILTER = "filter";
}
