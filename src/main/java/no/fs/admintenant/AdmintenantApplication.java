package no.fs.admintenant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.session.data.mongo.config.annotation.web.http.EnableMongoHttpSession;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"no.fs"})
@EnableMongoRepositories
@EnableMongoAuditing
public class AdmintenantApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdmintenantApplication.class, args);
	}


}
