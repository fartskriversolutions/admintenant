package no.fs.admintenant.config;

import no.fs.admintenant.config.aaa.AuditorAwareImpl;
import no.fs.admintenant.domain.model.ServiceTemplate;
import no.fs.admintenant.domain.model.User;
import no.fs.admintenant.domain.repositories.ServiceTemplateRepository;
import no.fs.admintenant.domain.services.UserManagementService;
import no.fs.admintenant.helpers.ResourceHelper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements InitializingBean {

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    AuditorAwareImpl auditorAware;

    @Autowired
    ServiceTemplateRepository serviceTemplateRepository;

    @Override
    public void afterPropertiesSet() throws Exception {
        User sa =userManagementService.getSystemService();
        synchronized (this){
            auditorAware.setTemporaryAuditor(sa);

            userManagementService.checkUsers();

            if(serviceTemplateRepository.count() < 1) {
                ServiceTemplate template = new ServiceTemplate();
                template.setName("default");
                template.setComposeTemplate(ResourceHelper.readResourceFile("/docker-compose/docker-compose.default.yml"));
                serviceTemplateRepository.save(template);
            }
            auditorAware.removeTemporaryAuditor();
        }
    }
}
