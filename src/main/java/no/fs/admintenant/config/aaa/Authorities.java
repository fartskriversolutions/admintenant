package no.fs.admintenant.config.aaa;

/**
 * Created by denz0x13 on 12.12.16.
 */
public enum Authorities {

    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER"),
    ANONYMOUS("ROLE_ANONYMOUS"),
    ;

    private final String roleName;

    Authorities(String role) {
        this.roleName = role;
    }

    public String getRoleName(){
        return roleName;
    }


}
