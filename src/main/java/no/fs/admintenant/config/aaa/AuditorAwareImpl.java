package no.fs.admintenant.config.aaa;

import no.fs.admintenant.domain.model.base.AuthEntity;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


@Component("auditorAware")
public class AuditorAwareImpl implements AuditorAware<AuthEntity> {
    private AuthEntity temporaryAuditor;

    @Override
    public AuthEntity getCurrentAuditor() {
        if (temporaryAuditor != null) {
            return temporaryAuditor;
        }
        try {
            return ((AuthEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        }catch (Exception e){

        }
        return null;
    }

    public void setTemporaryAuditor(AuthEntity auditor) {
        synchronized (this) {
            this.temporaryAuditor = auditor;
        }
    }

    public void removeTemporaryAuditor() {
        synchronized (this) {
            this.temporaryAuditor = null;
        }
    }

}
