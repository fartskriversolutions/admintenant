package no.fs.admintenant.config;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppDockerConfig {

    @Bean
    DockerClient dockerClient(){
        DockerClient dockerClient = DockerClientBuilder.getInstance().build();
        return dockerClient;
    }
}
