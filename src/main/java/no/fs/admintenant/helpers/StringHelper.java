package no.fs.admintenant.helpers;

import java.security.SecureRandom;

/**
 * Created by denz0x13 on 15.02.17.
 */
public class StringHelper {

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    public static String makeCSV(Object[][] table, String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < table.length; i++) {
            String[] _row = new String[table[i].length];
            for (int j = 0; j < table[i].length; j++) {
                _row[j] = table[i][j].toString();
            }
            sb.append(String.join(delimiter, _row)).append("\n");
        }

        return sb.toString();
    }



}
