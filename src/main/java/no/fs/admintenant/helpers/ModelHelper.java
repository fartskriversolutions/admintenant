package no.fs.admintenant.helpers;

import com.google.common.base.CaseFormat;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by denz0x13 on 20.03.17.
 */
public class ModelHelper {
    public static List<Long> extractModelId(Map<String, Object> params, Class model) {
        String[] _a = model.getName().split("\\.");
        String modelNameCamel = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, _a[_a.length - 1]);
        String modelNameLU = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, modelNameCamel);

        List<String> _keys = Arrays.asList(modelNameCamel + "Id", modelNameLU + "_id");

        for (String _key : _keys) {
            if (params.containsKey(_key)) {
                Object _result = null;
                if(params.get(_key) instanceof List){
                    _result = params.get(_key);
                }
                if(_result == null) {
                    _result = ExtractorHelper.extractObject(params.get(_key).toString());
                }
                if (_result instanceof List) {
                    List<Long> _r = new CopyOnWriteArrayList<>();
                    for (Object _o : (List) _result) {
                        if (_o instanceof Long) {
                            _r.add((Long) _o);
                        }
                    }
                    return _r;
                }
                if (_result instanceof Long) {
                    List<Long> _r = new CopyOnWriteArrayList<>();
                    _r.add((Long) _result);
                    return _r;
                }
            }
        }

        return null;

    }

    public static String shortModelName(Class model){
        String[] _a = model.getName().split("\\.");
        return _a[_a.length - 1];
    }

    public static String getModelId(Class klazz) {
        return getModelId(klazz.getName());
    }

    public static String getModelId(String modelName) {
        String[] _a = modelName.split("\\.");
        String modelNameCamel = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, _a[_a.length - 1]);
        String modelNameLU = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, modelNameCamel);
        return modelNameLU + "_id";
    }

}
