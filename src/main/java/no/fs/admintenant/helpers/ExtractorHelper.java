package no.fs.admintenant.helpers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import no.fs.admintenant.datatypes.AbstractJsonType;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by denz0x13 on 13.01.17.
 */
public class ExtractorHelper {

    static ObjectMapper objectMapper = new ObjectMapper();

    public static final Pattern integerPattern = Pattern.compile("-?\\d+");
    public static final Pattern floatPattern = Pattern.compile("-?\\d+\\.\\d+");
    public static final Pattern hashPattern = Pattern.compile("^[_,a-z,A-Z]+\\w+\\[\\w+\\]$");
    public static final Pattern hashArrayPattern = Pattern.compile("^[_,a-z,A-Z]+\\w+\\[\\w+\\]\\[\\]$");
    public static final Pattern arrayPattern = Pattern.compile("^[_,a-z,A-Z]+\\w+\\[\\]$");
    public static final Pattern validKeyPattern = Pattern.compile("^[_,a-z,A-Z]+\\w*$");
    public static final Pattern validJsonPattern = Pattern.compile("^\\{\\s*\\\".*\\}$|^\\[\\n?\\{\\s*\\\".*\\}\\n?\\]$");
    public static final Pattern validBooleanPattern = Pattern.compile("^(?:tru|fals)e$",Pattern.CASE_INSENSITIVE);

    public static Boolean isInteger(String source) {
        return integerPattern.matcher(source).matches();
    }

    public static Boolean isFloat(String source) {
        return floatPattern.matcher(source).matches() && !isInteger(source);
    }

    public static Boolean isHashKey(String source) {
        return hashPattern.matcher(source).matches();
    }

    public static Boolean isHashArrayKey(String source) {
        return hashArrayPattern.matcher(source).matches();
    }

    public static Boolean isArrayKey(String source) {
        return arrayPattern.matcher(source).matches();
    }

    public static Boolean isJson(String source) {
        return validJsonPattern.matcher(source).matches();
    }

    public static Boolean isBoolean(String source){
        return validBooleanPattern.matcher(source).matches();
    }

    public static Boolean isValidKey(String source) {
        return validKeyPattern.matcher(source).matches();
    }

    public static Object extractObject(String source) {
        if (ExtractorHelper.isInteger(source)) {
            try {
                return Long.parseLong(source);
            } catch (IllegalFormatException e) {
                return source;
            }
        } else if (ExtractorHelper.isFloat(source)) {
            try {
                return Double.parseDouble(source);
            } catch (IllegalFormatException e) {
                return source;
            }
        } else if(ExtractorHelper.isJson(source)) {
            try {
                return objectMapper.readValue(source,new TypeReference<Map<String,Object>>(){});
            } catch (IOException e) {

            }
        }
        if(source.contains(",")){
            return Arrays.asList(source.split(",")).stream().map(s -> extractObject(s)).collect(Collectors.toList());
        }
        return source;

    }

    public static Map<String, Object> extractFromMap(MultiValueMap<String, String> params) {
        List<String> _normilizedKeys = new ArrayList<>();
        params.keySet().stream().forEach(pKey -> {
            ParamInfo pInfo = new ParamInfo(pKey);
            if (pInfo.isValid()) {
                _normilizedKeys.add(pInfo.getK());
            }
        });
        return extractFromMap(_normilizedKeys, params);
    }

    public static Map<String, Object> extractFromMap(List<String> keys, MultiValueMap<String, String> params) {
        Map<String, Object> result = new LinkedHashMap<>();
        for (String pKey : params.keySet()) {
            ParamInfo pInfo = new ParamInfo(pKey);
            if (!pInfo.isValid()) continue;
            if (keys.contains(pInfo.getK())) {
                for (String v : params.get(pKey)) {
                    result = normalizeParams(result, pKey, v);
                }
            }
        }
        return result;
    }

    public static HashMap<String, Object> extractFromMapOld(List<String> keys, MultiValueMap<String, String> params) {
        HashMap<String, Object> _result = new HashMap<>();
        ArrayList<String> _array_keys = new ArrayList<String>();
        HashMap<String, List<String>> _unparsed_hash = new HashMap<>();
        for (String p : keys) {
            if (params.containsKey(p)) {
                if (isArrayKey(p)) {
                    String _new_key = p.replaceAll("\\[\\]", "");
                    if (!_array_keys.contains(_new_key)) {
                        _array_keys.add(_new_key);
                    }
                    continue;
                }
                if (isHashKey(p)) {
                    _unparsed_hash.put(p, params.get(p));
                    continue;
                }
                if (params.get(p).size() == 1) {
                    String _source = params.get(p).get(0);
                    _result.put(p, ExtractorHelper.extractObject(_source));
                } else if (params.get(p).size() >= 1) {
                    List<Object> _array = new ArrayList<Object>();
                    params.get(p).forEach(c -> {
                        _array.add(ExtractorHelper.extractObject(c));
                    });
                    _result.put(p, _array);
                }
            }
        }
        for (String p : _array_keys) {
            if (!_result.containsKey(p)) {
                _result.put(p, new ArrayList<Object>());
            }
            ((ArrayList<Object>) _result.get(p)).addAll(params.get(p + "[]"));
        }
        return _result;
    }


    public static AbstractJsonType asJsonType(MultiValueMap<String, String> params) {
        return asJsonType(Lists.newArrayList(params.keySet()), params);
    }

    public static AbstractJsonType asJsonType(List<String> keys, MultiValueMap<String, String> params) {
        return new AbstractJsonType() {
            private AbstractJsonType init(Map<String, Object> _props) {
                _props.forEach((k, v) -> setAdditionalProperty(k, v));
                return this;
            }
        }.init(extractFromMap(keys, params));
    }


    /**
     * @param query Now possible parse query like this - h[k]=1&h[p]=3&p[]=a&p[]=b&p[]=c&a=1&s=jdksjdks&h1[][p]=1&h1[][c]=2&h1[][p]=3
     * @return Map of parsed params
     */
    public static Map<String, Object> parseHTTPQueryString(String query) {
        return parseHTTPQueryString(query, new ArrayList<String>());
    }

    /**
     * @param query Now possible parse query like this - h[k]=1&h[p]=3&p[]=a&p[]=b&p[]=c&a=1&s=jdksjdks&h1[][p]=1&h1[][c]=2&h1[][p]=3
     * @param keys  extract only these keys
     * @return
     */
    public static Map<String, Object> parseHTTPQueryString(String query, List<String> keys) {
        Map<String, Object> result = new LinkedHashMap<>();
        if(query == null){
            return result;
        }
        if(keys==null){
            keys = new ArrayList<>();
        }
        try {
            String[] _q = URLDecoder.decode(query, "UTF-8").split("&");
            for (int i = 0; i < _q.length; i++) {
                String _kv[] = _q[i].split("=");
                if (_kv.length >= 2) {
                    if (keys.isEmpty()) {
                        result = normalizeParams(result, _kv[0], _kv[1]);
                    } else {
                        ParamInfo paramInfo = new ParamInfo(_kv[0]);
                        if (!paramInfo.isValid()) continue;
                        if (keys.contains(paramInfo.getK())) {
                            result = normalizeParams(result, _kv[0], _kv[1]);
                        }
                    }
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Map<String, Object> parseHTTPQueryString(String query, String... keys){
        return parseHTTPQueryString(query,Arrays.asList(keys));
    }


    public static Map<String, Object> normalizeParams(Map<String, Object> params, String name, Object v) {
        //String p1 = "\\A[\\[\\]]*([^\\[\\]]+)\\]*";
        String p2 = "^\\[\\]\\[([^\\[\\]]+)\\]$";
        //String p3 = "^\\[\\](.+)$";
        String _hash_pattern = "^\\[([^\\[\\]]+)\\]$";

        ParamInfo paramInfo = new ParamInfo(name);
        if (!paramInfo.isValid()) {
            return params;
        }

        String k = paramInfo.getK();
        String after = paramInfo.getAfter();

        Object _v = v instanceof String ? extractObject((String) v) : v;

        if (after.isEmpty()) {
            params.putIfAbsent(k, _v);
        } else if (after.equals("[]") && after.length() == 2) {
            List<Object> _a = null;
            if (params.containsKey(k)) {
                if (params.get(k) instanceof List) {
                    _a = (List) params.get(k);
                    _a.add(v);

                }
            } else {
                _a = new ArrayList<Object>();
                _a.add(_v);
                params.put(k, _a);
            }


        } else {
            List<String> _h_m = getMatchers(_hash_pattern, after);
            if (_h_m.size() == 1) {
                String _newKey = _h_m.get(0);
                if (isValidKey(_newKey)) {
                    try {
                        Map<String, Object> _hash = (Map) params.get(k);
                        if (_hash == null) {
                            _hash = new HashMap<String, Object>();
                        }
                        _hash.putIfAbsent(_newKey, _v);
                        params.put(k, _hash);
                    } catch (ClassCastException ex) {
                        ex.printStackTrace();
                    }
                }


            } else {
                //TODO need implement recursion to parse nested arrays and hashes like [][key],[key][],[key][key] ...etc

                //Parsing [][key]
                List<String> _aofh_m = getMatchers(p2, after);
                if (_aofh_m.size() > 0) {
                    String _newKey = _aofh_m.get(0);
                    if (isValidKey(_newKey)) {
                        List<Object> _aofh_a = null;
                        if (params.get(k) != null) {
                            if (params.get(k) instanceof List) {
                                _aofh_a = (List) params.get(k);
                            } else {
                                return params;
                            }
                        } else {
                            _aofh_a = new Vector<Object>();
                            params.put(k, _aofh_a);
                        }

                        for (Object _item : _aofh_a) {
                            if (_item instanceof Map) {
                                if (!((Map) _item).containsKey(_newKey)) {
                                    ((Map) _item).put(_newKey, _v);
                                    return params;
                                }
                            }
                        }

                        HashMap _hash = new HashMap<String, Object>();
                        _hash.put(_newKey, _v);
                        _aofh_a.add(_hash);
                        return params;


                    }
                }

            }

        }


        return params;
    }

    public static List<String> getMatchers(String strPattern, String context) {
        List<String> _result = new ArrayList<>();
        Pattern pattern = Pattern.compile(strPattern);
        Matcher matcher = pattern.matcher(context);
        while (matcher.find()) {
            _result.add(matcher.group(1));
        }
        return _result;
    }

    public static class ParamInfo {

        public final String patternStr = "\\A[\\[\\]]*([^\\[\\]]+)\\]*";
        private String k;
        private String after;


        public ParamInfo(String raw) {
            k = "";
            after = "";
            List<String> _m = getMatchers(patternStr, raw);
            if (_m.size() > 0) {
                k = _m.get(0);
            }
            String[] _r = raw.split(k);
            if (_r.length > 0) {
                after = _r[1];
            }
        }

        public String getK() {
            return k;
        }

        public String getAfter() {
            return after;
        }

        public Boolean isValid() {
            return !k.isEmpty();
        }
    }

}
