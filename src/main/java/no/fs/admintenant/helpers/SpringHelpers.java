package no.fs.admintenant.helpers;

import org.springframework.beans.*;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

import static java.util.Arrays.stream;

/**
 * Almost the same as Spring BeanUtils.copyProperties(...) except possibility
 * to ignore null properties from source object
 */
public class SpringHelpers {

    public static void copyProperties(Object source, Object target, boolean copySourceNulls, String... ignoreProperties)
            throws BeansException {

        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");

        final BeanWrapper src = new BeanWrapperImpl(source);
        final BeanWrapper trg = new BeanWrapperImpl(target);

        PropertyDescriptor[] targetPds = trg.getPropertyDescriptors();
        List<String> ignoreList = (ignoreProperties != null ? Arrays.asList(ignoreProperties) : null);

        for (PropertyDescriptor targetPd : targetPds) {
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null && (ignoreList == null || !ignoreList.contains(targetPd.getName()))) {
                PropertyDescriptor sourcePd = null;
                try {
                    sourcePd = src.getPropertyDescriptor(targetPd.getName());
                } catch (InvalidPropertyException ex) {
                }
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();
                    if (readMethod != null &&
                            ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }
                            Object value = readMethod.invoke(source);
                            if (copySourceNulls || value != null) {
                                if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                    writeMethod.setAccessible(true);
                                }
                                writeMethod.invoke(target, value);
                            }

                        } catch (Throwable ex) {
                            throw new FatalBeanException(
                                    "Could not copy property '" + targetPd.getName() + "' from source to target", ex);
                        }
                    }
                }
            }
        }
    }

    public static Set<Class<?>> getAnnotatedClasses(Class... annotations) {
        Set<Class<?>> _result = new HashSet<>();
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        if (annotations.length < 1) {
            return _result;
        }
        for (int i = 0; i < annotations.length; i++) {
            scanner.addIncludeFilter(new AnnotationTypeFilter(annotations[i]));
        }
        Set<BeanDefinition> components = scanner.findCandidateComponents("no.fs");
        for (BeanDefinition bd : components) {
            try {
                Class<?> klass = Class.forName(bd.getBeanClassName());
                _result.add(klass);
            } catch (ClassNotFoundException e) {
                //e.printStackTrace();
            }
        }
        return _result;
    }





    private static Class _getActualType(Type returnType) {
        if (returnType instanceof ParameterizedType) {
            ParameterizedType type = (ParameterizedType) returnType;
            Type[] typeArguments = type.getActualTypeArguments();
            if (typeArguments.length > 0) {
                return (Class) typeArguments[0];
            }
        }

        return null;
    }







    public static Boolean isSuperClassFor(Class<?> superClass,Class<?> childClass){
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(true);
        provider.addIncludeFilter(new AssignableTypeFilter(superClass));

        Set<BeanDefinition> components = provider.findCandidateComponents("no.fs");
        for (BeanDefinition component : components)
        {
            try {
                Class cls = Class.forName(component.getBeanClassName());
                if(childClass.equals(cls)){
                    return true;
                }
            } catch (ClassNotFoundException e) {
                //e.printStackTrace();
            }
            // use class cls found
        }
        return false;
    }

}
