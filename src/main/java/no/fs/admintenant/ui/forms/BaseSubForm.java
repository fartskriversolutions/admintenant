package no.fs.admintenant.ui.forms;

import com.vaadin.data.HasValue;
import com.vaadin.shared.Registration;
import no.fs.admintenant.domain.model.base.BaseEntity;
import org.vaadin.viritin.form.AbstractForm;

/**
 * Created by denz0x13 on 12.10.2017.
 */
public abstract class BaseSubForm<E extends BaseEntity> extends AbstractForm<E> implements HasValue<E> {

    public BaseSubForm(Class<E> entityType) {
        super(entityType);
        getBinder().addValueChangeListener(e -> {
            getListeners(ValueChangeEvent.class).forEach(l -> ((ValueChangeListener) l).valueChange(e));
        });
    }

//    public BaseSubForm(){
//        this((Class<E>) new TypeToken<E>(getClass()){}.getRawType());
//    }

    public BaseSubForm(Class<E> entityType, String caption) {
        this(entityType);
        setCaption(caption);
    }


    @Override
    public void setValue(E value) {
        this.setEntity(value);
    }

    @Override
    public E getValue() {
        return this.getEntity();
    }

    @Override
    public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {

    }

    @Override
    public boolean isRequiredIndicatorVisible() {
        return false;
    }

    @Override
    public void setReadOnly(boolean readOnly) {

    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<E> listener) {
        return addListener(ValueChangeEvent.class, listener, ValueChangeListener.VALUE_CHANGE_METHOD);
    }

}
