package no.fs.admintenant.ui.forms;

import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import no.fs.admintenant.domain.model.base.BaseEntity;
import no.fs.admintenant.domain.services.BaseService;
import org.vaadin.viritin.form.AbstractForm;

/**
 * Created by denz0x13 on 11.08.17.
 */
public abstract class BaseForm<E extends BaseEntity,S extends BaseService> extends AbstractForm<E> {
    private S service;
    protected Window popup;
    private BaseForm(Class<E> entityType) {
        super(entityType);
    }

    public BaseForm(Class<E> entityType, S service){
        this(entityType);
        this.service = service;
    }

    public BaseForm(S service){
        this(service.getModelClass());
        this.service = service;
    }

    public S getService(){
        return service;
    }


    @Override
    public E getEntity() {
        return super.getEntity();
    }

    @Override
    public void setEntity(E entity) {
        super.setEntity(entity);
    }

    public void setCaption(String caption) {
        setModalWindowTitle(caption);
    }

    public void refresh(){

    }

    public Window openInModalPopup() {
        popup = new Window(getModalWindowTitle());
        popup.setModal(true);
        adjustModalPopup(popup);
        popup.setContent(this);
        UI.getCurrent().addWindow(popup);
        focusFirst();
        return popup;
    }

    protected void adjustModalPopup(Window window){

    }

    @Override
    public Window getPopup() {
        return this.popup;
    }

    @Override
    public void closePopup() {
        if(getPopup() != null) {
            getPopup().close();
        }
    }
}
