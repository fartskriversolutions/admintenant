package no.fs.admintenant.ui.components;


import com.vaadin.ui.Component;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MPanel;

public class InfoBox extends MPanel {

    protected MFormLayout contentLayout;

    public InfoBox(){
        super();
        setSizeUndefined();
        contentLayout = new MFormLayout().withUndefinedSize().withMargin(true);
        setContent(contentLayout);
    }

    public InfoBox(String caption){
        this();
        super.setCaption(caption);
    }

    public InfoBox add(Component component){
        contentLayout.addComponent(component);
        return this;
    }

    public InfoBox add(Component... components){
        contentLayout.addComponents(components);
        return this;
    }

    public Component getContentLayout(){
        return contentLayout;
    }

    public void buildLayout(){

    }
}
