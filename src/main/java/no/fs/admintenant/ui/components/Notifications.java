package no.fs.admintenant.ui.components;

import com.vaadin.server.Page;
import com.vaadin.ui.Notification;

/**
 * Created by denz0x13 on 14.08.17.
 */
public class Notifications {
    public static void showNotification(Notification.Type type, String message) {
        new Notification(message, type).show(Page.getCurrent());
    }

    public static void showError(String message) {
        showNotification(Notification.Type.ERROR_MESSAGE, message);
    }

    public static void showMessage(String message) {
        showNotification(Notification.Type.HUMANIZED_MESSAGE, message);
    }


}
