package no.fs.admintenant.ui.components;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.themes.ValoTheme;
import no.fs.admintenant.domain.model.base.BaseEntity;
import no.fs.admintenant.domain.services.BaseService;
import no.fs.admintenant.helpers.ModelHelper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.vaadin.viritin.grid.MGrid;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by denz0x13 on 11.08.17.
 */
public class BaseGrid<E extends BaseEntity,S extends BaseService<E,?>> extends MGrid<E> {

    public interface CrudClickCallback<E extends BaseEntity> extends Serializable {
        void onClick(E item);
    }

    private S service;
    private Map<String,Object> params;

    private final List<BaseGrid> children = new LinkedList<>();

    public BaseGrid(S service){
        super(service.getModelClass());
        this.service = service;
        params = new HashMap<>();

        init();
    }

    public BaseGrid(S service, BaseGrid parent) {
        super(service.getModelClass());
        this.service = service;
        params = new HashMap<>();
        parent.appendChildGrid(this);
        init();
    }

    public BaseGrid(S service, Map<String, Object> newParams) {
        super(service.getModelClass());
        this.params = newParams;
        this.service = service;
        init();
    }

    public S getService(){
        return service;
    }

    public void setParams(Map<String,Object> newParams){
        params = newParams;
        refreshData();
    }

    public void addParams(Map<String, Object> newParams) {
        if (params == null) {
            params = newParams;
        } else {
            params.putAll(newParams);
        }
        refreshData();
    }

    public Map<String,Object> getParams(){
        return this.params;
    }


    protected void init(){
        this.setDataProvider(
                (List<QuerySortOrder> sortOrder, int offset, int limit) ->
                        getEntities(sortOrder,offset,limit).stream(),
                () -> countEntities()
        );

        this.addItemClickListener(new ItemClickListener() {

            @Override
            public void itemClick(ItemClick event) {
                BaseEntity entity = (BaseEntity) event.getItem();
                for (BaseGrid childGrid : children) {
                    childGrid.setParams(new HashMap<String, Object>() {{
                        put(ModelHelper.getModelId(service.getModelName()), entity.getId());
                    }});
                }
                if (children.size() > 0) {
                    UI.getCurrent().scrollIntoView(children.get(0));
                }
            }
        });
    }


    public void refreshData() {
        if (this.getDataProvider() != null) {

            this.getDataProvider().refreshAll();

            for (BaseGrid childGrid : children) {
                childGrid.setParams(new HashMap<String, Object>() {{
                    put(ModelHelper.getModelId(service.getModelName()), 0L);
                }});
            }
        }
    }

    protected List getEntities(List<QuerySortOrder> sortOrder, int offset, int limit){
        List _r = service.findAll(getParams(),
                new PageRequest(
                        offset/limit,
                        limit,
                        sortOrder.isEmpty() || sortOrder.get(0).getDirection() == SortDirection.ASCENDING ? Sort.Direction.ASC : Sort.Direction.DESC,
                        sortOrder.isEmpty() ? "createdAt" : sortOrder.get(0).getSorted()
                ))
                .getContent();
        return _r;
    }

    protected int countEntities(){
        return service.count(getParams()).intValue();
    }

    public void reload(){
        init();
    }

    @Override
    public BaseGrid<E,S> withProperties(String... properties) {
        return (BaseGrid<E,S>)super.withProperties(properties);
    }


    @Override
    public BaseGrid<E,S> withColumnHeaders(String... properties) {
        return (BaseGrid<E,S>)super.withColumnHeaders(properties);
    }

    @Override
    public BaseGrid<E,S> setRows(List<E> rows) {
        return (BaseGrid<E,S>)super.setRows(rows);
    }

    public Column addActionColumn(final String name, final CrudClickCallback<E> callback) {
        return addComponentColumn(e -> {
            Button button = new Button(name, a -> {
                callback.onClick(e);
            });
            button.setStyleName(MaterialTheme.BUTTON_SMALL);
            return button;
        });
    }

    public Column addEditColumn(final CrudClickCallback<E> callback) {
        return addComponentColumn(e -> {
            Button button = new Button("Edit", a -> {
                callback.onClick(e);
            });
            button.setStyleName(MaterialTheme.BUTTON_SMALL);
            button.setEnabled(getService().canUpdate(e.getId()));
            return button;
        });
    }

    public Column addDeleteColumn(final CrudClickCallback<E> callback) {
        return addComponentColumn(e -> {
            Button button = new Button("Delete", a -> {
                callback.onClick(e);
            });
            button.setStyleName(MaterialTheme.BUTTON_SMALL);
            button.setEnabled(getService().canDelete(e.getId()));
            return button;
        });
    }

    public void appendChildGrid(final BaseGrid grid) {
        this.children.add(grid);
        grid.setParams(new HashMap<String, Object>() {{
            put(ModelHelper.getModelId(service.getModelName()), 0L);
        }});
    }

    public E getSelectedItem(){
        return getSelectedItems().stream().findFirst().orElse(null);
    }
}
