package no.fs.admintenant.ui.data;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.DataProviderListener;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.Registration;
import com.vaadin.shared.data.sort.SortDirection;
import no.fs.admintenant.domain.model.base.BaseEntity;
import no.fs.admintenant.domain.services.BaseService;
import no.fs.admintenant.domain.services.exeptions.ServiceException;
import org.springframework.data.domain.PageRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by denz0x13 on 11.10.2017.
 */
public class EntityDataProvider<E extends BaseEntity> implements DataProvider<E, QuerySortOrder> {

    private BaseService service;

    public EntityDataProvider(BaseService<E, ?> service) {
        this.service = service;
    }

    protected Map<String, Object> getParams() {
        return new HashMap<>();
    }

    @Override
    public boolean isInMemory() {
        return false;
    }

    @Override
    public int size(Query<E, QuerySortOrder> query) {
        return service.count(getParams()).intValue();
    }

    @Override
    public Stream<E> fetch(Query<E, QuerySortOrder> query) {
        List<QuerySortOrder> sortOrder = query.getSortOrders();
        List<E> _r = service.findAll(getParams(),
                new PageRequest(
                        query.getOffset() / query.getLimit(),
                        query.getLimit(),
                        sortOrder.isEmpty() || sortOrder.get(0).getDirection() == SortDirection.ASCENDING ? org.springframework.data.domain.Sort.Direction.ASC : org.springframework.data.domain.Sort.Direction.DESC,
                        sortOrder.isEmpty() ? "createdAt" : sortOrder.get(0).getSorted()
                ))
                .getContent();

        return _r.stream();
    }

    @Override
    public void refreshItem(E item) {
        try {
            item = (E) service.findOne(item.getId());
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refreshAll() {

    }

    @Override
    public Registration addDataProviderListener(DataProviderListener<E> listener) {
        return null;
    }
}
