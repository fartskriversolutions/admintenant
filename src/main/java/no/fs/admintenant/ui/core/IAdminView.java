package no.fs.admintenant.ui.core;

import com.vaadin.navigator.View;
import com.vaadin.ui.Button;
import no.fs.admintenant.domain.model.base.BaseEntity;
import no.fs.admintenant.domain.services.BaseService;
import no.fs.admintenant.domain.services.exeptions.ServiceException;
import no.fs.admintenant.ui.components.Notifications;
import no.fs.admintenant.ui.forms.BaseForm;
import org.vaadin.viritin.button.MButton;

/**
 * Created by denz0x13 on 23.12.15.
 */
public interface IAdminView<E extends BaseEntity, S extends BaseService<E, ?>> extends View {
    public void reload();

    S getService();

    BaseForm<E, S> getEditForm();

    default Button newEntityButton(String caption) {
        if (getEditForm() == null) {
            return null;
        }
        return new MButton(caption, e -> {
            getEditForm().setEntity(getService().newEntity());
            getEditForm().openInModalPopup();
        });
    }

    default void saveEntity(E entity) {
        try {
            getService().save(entity);
        }catch (Exception ex){
            Notifications.showError(ex.getMessage());
            return;
        }
        getEditForm().getPopup().close();
        reload();
    }

    default void deleteEntity(E entity) {
        if (entity == null) {
            return;
        }
        if (entity.getId() == null) {
            return;
        }
        if (getService().canDelete(entity.getId())) {
            getService().delete(entity.getId());
            reload();
        }
    }

    default void editEntity(E entity) {
        String id = entity.getId();
        if (id != null) {
            try {
                getEditForm().setEntity(getService().findOne(id));
                getEditForm().openInModalPopup();
            } catch (Exception e) {
                Notifications.showError(e.getMessage());
            }
        }
    }

    default void newEntityForm() {
        getEditForm().refresh();
        getEditForm().setEntity(getService().newEntity());
        getEditForm().openInModalPopup();
    }

    default void resetEntity(E entity) {
        getEditForm().getPopup().close();
        reload();
    }
}
