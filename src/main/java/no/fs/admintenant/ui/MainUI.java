package no.fs.admintenant.ui;

import com.github.appreciated.app.layout.behaviour.AppLayout;
import com.github.appreciated.app.layout.behaviour.Behaviour;
import com.github.appreciated.app.layout.builder.AppLayoutBuilder;
import com.github.appreciated.app.layout.builder.design.AppBarDesign;
import com.github.appreciated.app.layout.builder.elements.SubmenuBuilder;
import com.github.appreciated.app.layout.builder.providers.DefaultSpringNavigationElementInfoProvider;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.*;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.*;
import no.fs.admintenant.domain.services.UserManagementService;
import no.fs.admintenant.ui.view.home.HomeView;
import no.fs.admintenant.ui.view.SSLSertivicates.SSLCertAdminView;
import no.fs.admintenant.ui.view.services.ServicesView;
import no.fs.admintenant.ui.view.services.templates.ServiceTemplateView;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.security.VaadinSecurity;

@SpringUI(path = "/")
@Theme("admintenant")
//@Widgetset("no.fs.ui.admintenat.widgetset")
public class MainUI extends UI implements ClientConnector.DetachListener {

    public static final String menuCaption = "Admin";

    @Autowired
    VaadinSecurity vaadinSecurity;

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    SpringNavigator navigator;

    @Autowired
    private SpringViewProvider viewProvider;


    AppLayout appLayout;

    @Override
    protected void init(VaadinRequest request) {
        getAppLayout();
        setNavigator(navigator);
        setContent(appLayout);
    }



    private AppLayout getAppLayout(){
        if(this.appLayout!=null){
            return this.appLayout;
        }

        appLayout = AppLayoutBuilder.get(Behaviour.LEFT_RESPONSIVE_HYBRID)
                .withTitle(menuCaption)
                .withDesign(AppBarDesign.MATERIAL)
                .withCDI(true)
                .withNavigationElementInfoProvider(new DefaultSpringNavigationElementInfoProvider())
                //.add(new MenuHeader("Tenant admin","0.0.1",VaadinIcons.SERVER))
                .add(HomeView.class)
                .add(SubmenuBuilder.get("Server",VaadinIcons.SERVER)
                        .add(ServicesView.class)
                        .add(ServiceTemplateView.class)
                        .build()
                )
                .add(SSLCertAdminView.class)
                .addClickable("Log out",VaadinIcons.SIGN_OUT, e -> vaadinSecurity.logout(), AppLayoutBuilder.Position.FOOTER)
                .withNavigatorProducer(components -> {
                    navigator.init(this,components);
                    return navigator;
                })
                //.withDefaultNavigationView(HomeView.class)
                .build();

        return this.appLayout;

    }


    @Override
    public void detach(DetachEvent event) {
        getUI().close();
    }
}
