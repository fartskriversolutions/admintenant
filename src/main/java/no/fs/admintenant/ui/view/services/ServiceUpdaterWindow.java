package no.fs.admintenant.ui.view.services;


import com.vaadin.ui.*;
import no.fs.admintenant.domain.model.ServiceInstance;
import no.fs.admintenant.services.DockerService;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ServiceUpdaterWindow extends Window{

    private MVerticalLayout mainLayout;
    private TextArea outputLog;
    private ProgressBar progressBar;
    private DockerService dockerService;
    private List<ServiceInstance> serviceInstances;
    private Label status;
    private List<Runnable> afterFinishCallback;

    public ServiceUpdaterWindow(DockerService dockerService) {
        super();
        this.dockerService = dockerService;
        setCaption("Service update");
        setWidth(80,Unit.PERCENTAGE);
        setHeight(80,Unit.PERCENTAGE);
        center();
        setModal(true);
        outputLog = new TextArea();
        outputLog.setSizeFull();
        progressBar = new ProgressBar(new Float(0.0));
        progressBar.setWidth(100,Unit.PERCENTAGE);
        status = new Label();
        mainLayout = new MVerticalLayout().withFullSize().withMargin(true);
        this.serviceInstances = new ArrayList<>();
        buildLayout();
        afterFinishCallback =new LinkedList<>();
    }

    public ServiceUpdaterWindow(DockerService dockerService, List<ServiceInstance> serviceInstances){
        this(dockerService);
        this.serviceInstances = serviceInstances;
    }

    public void setServiceInstances(List<ServiceInstance> serviceInstances){
        this.serviceInstances = serviceInstances;
    }

    private void buildLayout(){
        mainLayout.add(progressBar);
        mainLayout.setExpandRatio(progressBar,0.025f);
        mainLayout.add(status);
        mainLayout.setExpandRatio(status,0.025f);
        mainLayout.add(outputLog);
        mainLayout.setExpandRatio(outputLog,0.95f);
        setContent(mainLayout);
    }

    public void start(){
        if(this.serviceInstances.size() < 1){
            return;
        }
        setClosable(false);
        final WorkThread thread = new WorkThread();
        UI.getCurrent().setPollInterval(500);
        UI.getCurrent().addWindow(this);
        thread.start();

    }

    void addToLogOut(String line){
        outputLog.setValue(String.format("%s\n[%s] %s ",outputLog.getValue(), Instant.now().toString(),line));
    }

    public void addAfterFinishCallback(final Runnable callback){
        afterFinishCallback.add(callback);
    }

    class WorkThread extends Thread {
        volatile double current = 0.0;
        volatile double step = 1.0/serviceInstances.size();

        final Queue<ServiceInstance> queue = new LinkedList(serviceInstances);
        @Override
        public void run() {
            getUI().access(()->{
                progressBar.setValue(new Float(current));
                status.setValue("" + ((int)(current*100)) + "% done");
            });
            while (queue.size()>0){
                current+=step;
                ServiceInstance serviceInstance = queue.poll();
                getUI().access(()->{
                    addToLogOut(String.format("Processing: %s",serviceInstance.getDockerServiceName()));
                });
                dockerService.deployStack(serviceInstance);
                List<String> _out = dockerService.getLastCmdOut();
                if(_out.size()>0) {
                    getUI().access(() -> {
                        _out.forEach(_l-> addToLogOut(_l));
                        addToLogOut(String.format("Finished with code: %d",dockerService.getLastCmdExitCode()));
                    });
                }
                getUI().access(()->{
                    progressBar.setValue(new Float(current));
                        status.setValue("" + ((int)(current*100)) + "% done");
                });
            }

            getUI().access(()->{
                getUI().setPollInterval(-1);
                setClosable(true);
                afterFinishCallback.forEach( r -> run());
            });
        }
    }
}
