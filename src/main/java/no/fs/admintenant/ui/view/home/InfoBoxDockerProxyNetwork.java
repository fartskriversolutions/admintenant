package no.fs.admintenant.ui.view.home;

import com.github.appreciated.material.MaterialTheme;
import no.fs.admintenant.services.events.DockerServiceState;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.label.MLabel;

@Component
public class InfoBoxDockerProxyNetwork extends InfoBoxDocker {
    @Override
    public String getTitle() {
        return "Proxy network info";
    }

    @Override
    public DockerServiceState getDockerServiceState() {
        return DockerServiceState.PROXY_NETWORK;
    }

    @Override
    public void buildLayout() {
        super.buildLayout();
        if (!dockerService.isProxyNetworkPresent()) {
            add(
                    new MLabel("Proxy network not present!").withStyleName(MaterialTheme.LABEL_FAILURE)
            );
        } else {
            add(
                    new MLabel("Proxy network ID", dockerService.getProxyNetwork().getId())

            );
        }
    }
}
