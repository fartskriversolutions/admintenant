package no.fs.admintenant.ui.view.services.templates;

import com.vaadin.event.Action;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import no.fs.admintenant.domain.model.ServiceTemplate;
import no.fs.admintenant.domain.services.ServiceTemplateService;
import no.fs.admintenant.ui.forms.BaseForm;
import org.vaadin.aceeditor.AceEditor;
import org.vaadin.aceeditor.AceMode;
import org.vaadin.aceeditor.AceTheme;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

public class ServiceTemplateForm extends BaseForm<ServiceTemplate,ServiceTemplateService> {
    MTextField name = new MTextField("Name");
    MTextField description = new MTextField("Description");
    AceEditor composeTemplate = new AceEditor();



    public ServiceTemplateForm(ServiceTemplateService service) {
        super(service);
    }

    @Override
    protected Component createContent() {
        composeTemplate.setMode(AceMode.yaml);
        composeTemplate.setTheme(AceTheme.chrome);
        composeTemplate.setUseWorker(true);
        composeTemplate.setWidth(100,Unit.PERCENTAGE);
        composeTemplate.setHeight(370,Unit.PIXELS);
        getSaveButton().removeClickShortcut();
        return new MVerticalLayout(
                new MFormLayout(
                        name,
                        description,
                        composeTemplate
                        ).withMargin(true).withFullSize(),
                getToolbar()
        ).withFullSize();
    }

    @Override
    protected void adjustModalPopup(Window window) {
        window.setWidth(80,Unit.PERCENTAGE);
        window.setHeight(80,Unit.PERCENTAGE);
    }
}
