package no.fs.admintenant.ui.view.services;

import com.github.appreciated.app.layout.annotations.MenuCaption;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import no.fs.admintenant.domain.model.ServiceInstance;
import no.fs.admintenant.domain.services.ServiceInstanceService;
import no.fs.admintenant.domain.services.ServiceSSLCertContentService;
import no.fs.admintenant.domain.services.ServiceTemplateService;
import no.fs.admintenant.services.DockerService;
import no.fs.admintenant.services.events.DockerStackProcessEvent;
import no.fs.admintenant.ui.components.BaseGrid;
import no.fs.admintenant.ui.components.Notifications;
import no.fs.admintenant.ui.core.IAdminView;
import no.fs.admintenant.ui.forms.BaseForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

@SpringView(name = ServicesView.NAME)
@MenuCaption("Services")  // Set the Caption for the NavigationButton
@MenuIcon(VaadinIcons.CLUSTER) // Set the Icon for the NavigationButton
public class ServicesView extends MVerticalLayout implements IAdminView<ServiceInstance, ServiceInstanceService>, EventBusListener<DockerStackProcessEvent> {
    public final static String NAME = "Services";

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DockerService dockerService;

    @Autowired
    ServiceInstanceService serviceInstanceService;

    @Autowired
    ServiceTemplateService serviceTemplateService;

    @Autowired
    ServiceSSLCertContentService serviceSSLCertContentService;

    @Autowired
    EventBus.ApplicationEventBus applicationEventBus;

    TextArea outputLogs = new TextArea();

    ServiceForm serviceForm;

    BaseGrid servicesGrid;

    @PostConstruct
    void init() {
        applicationEventBus.subscribe(this);
        outputLogs.setSizeFull();
        addComponent(new Header("Service admin"));

        MHorizontalLayout _toolbox = new MHorizontalLayout();
        _toolbox.add(new MButton("New service", a -> newEntityForm()).withStyleName(MaterialTheme.BUTTON_PRIMARY));
        _toolbox.add(new MButton("Update/Deploy", a -> deployService()).withStyleName(MaterialTheme.BUTTON_PRIMARY));
        _toolbox.add(new MButton("Stop", a -> stopService()).withStyleName(MaterialTheme.BUTTON_PRIMARY));
        _toolbox.add(new MButton("Remove", a -> removeService()).withStyleName(MaterialTheme.BUTTON_DANGER));
        addComponent(_toolbox);

        servicesGrid = new BaseGrid<ServiceInstance, ServiceInstanceService>(serviceInstanceService);

        servicesGrid.withProperties(
                "name",
                "domainName",
                "deployed",
                "serviceSSLCertContent",
                "createdAt", "updatedAt").withFullWidth();

        servicesGrid.addEditColumn(e -> editEntity((ServiceInstance) e));
        //servicesGrid.addDeleteColumn(e -> deleteEntity((ServiceInstance) e));
//        servicesGrid.addComponentColumn(e -> {
//            Button button = new Button(((ServiceInstance) e).getDeployed() ? "Update":"Deploy", a -> {
//               deployService((ServiceInstance) e);
//            });
//            button.setStyleName(MaterialTheme.BUTTON_SMALL);
//            return button;
//        });

        servicesGrid.setFrozenColumnCount(3);

        addComponent(servicesGrid);
    }

    @PreDestroy
    void destroy() {
        applicationEventBus.unsubscribe(this);
    }

    private void deployService(){
        if(getCurrentService()==null){
            Notifications.showError("No service selected!");
            return;
        }
        deployService(getCurrentService());
        reload();
    }

    private void deployService(ServiceInstance serviceInstance) {
        dockerService.reload();
        if (!dockerService.getDockerEngineIsPresent()) {
            Notifications.showError("Start or setup docker engine first!");
            return;
        }
        if (!dockerService.isProxyNetworkPresent()) {
            Notifications.showError("Create proxy network first!");
            return;
        }

        ServiceUpdaterWindow serviceUpdaterWindow = new ServiceUpdaterWindow(dockerService, Arrays.asList(serviceInstance));
        //dockerService.deployStack(serviceInstance);
        serviceUpdaterWindow.start();
        serviceUpdaterWindow.addCloseListener( l -> {
           reload();
        });

    }

    private Window showLogOutputWindow() {
        Window deployOutput = new Window("Process output");
        deployOutput.setWidth(80, Unit.PERCENTAGE);
        deployOutput.setHeight(80, Unit.PERCENTAGE);
        deployOutput.center();
        MVerticalLayout content = new MVerticalLayout().withFullSize().withMargin(true);
        deployOutput.setModal(true);
        deployOutput.setContent(content);
        outputLogs.setValue("");
        content.add(outputLogs);
        UI.getCurrent().addWindow(deployOutput);
        return deployOutput;
    }

     ServiceInstance getCurrentService() {
         return (ServiceInstance) servicesGrid.getSelectedItems().stream().findFirst().orElse(null);

     }
    void stopService(){
        ServiceInstance serviceInstance = getCurrentService();
        if(serviceInstance!=null){
            dockerService.stopStack(serviceInstance);

        }else{
            Notifications.showError("Select service");
        }
    }

    void removeService() {
        ServiceInstance serviceInstance = getCurrentService();
        if(serviceInstance!=null){
            dockerService.deleteStack(serviceInstance);
            MessageBox.createQuestion()
                    .withMessage("Delete service info from database also?")
                    .withYesButton(()->{
                        serviceInstanceService.delete(serviceInstance.getId());

                    })
                    .withNoButton(ButtonOption.focus())
                    .open();
            reload();
        }else{
            Notifications.showError("Select service");
        }
    }

    public Boolean stackIsRunning(){
        return true;
    }

    @Override
    public void reload() {
        servicesGrid.getDataProvider().refreshAll();
    }

    @Override
    public ServiceInstanceService getService() {
        return serviceInstanceService;
    }

    @Override
    public BaseForm<ServiceInstance, ServiceInstanceService> getEditForm() {
        if (serviceForm != null) {
            return serviceForm;
        }

        serviceForm = new ServiceForm(serviceInstanceService,
                serviceTemplateService, serviceSSLCertContentService);
        serviceForm.setSavedHandler(this::saveEntity);
        serviceForm.setResetHandler(this::resetEntity);
        return serviceForm;
    }

    @Override
    public void onEvent(org.vaadin.spring.events.Event<DockerStackProcessEvent> event) {
        StreamGobbler sg = new StreamGobbler(event.getPayload().getIs(), outputLogs);
        getUI().access(sg);
    }

    class StreamGobbler extends Thread {
        InputStream is;
        AbstractField field;
        Boolean isRunning;

        StreamGobbler(InputStream is, AbstractField field) {
            this.is = is;
            this.field = field;
            isRunning = false;
        }

        @Override
        public void run() {
            isRunning = true;
            try {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    logger.info(line);
                    sb.append(line);
                    sb.append("\n");
                    getUI().access(new Runnable() {
                        @Override
                        public void run() {
                            field.setValue(sb.toString());
                        }
                    });

                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            isRunning = false;
        }

        public Boolean isRunning() {
            return isRunning;
        }
    }

}
