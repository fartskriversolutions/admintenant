package no.fs.admintenant.ui.view.SSLSertivicates;

import com.vaadin.ui.*;
import no.fs.admintenant.domain.model.ServiceSSLCertContent;
import no.fs.admintenant.domain.services.ServiceSSLCertContentService;
import no.fs.admintenant.ui.forms.BaseForm;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.io.*;

public class SSLCertificateForm extends BaseForm<ServiceSSLCertContent, ServiceSSLCertContentService> {

    MTextField name = new MTextField("Name");
    DateField expirationDate = new DateField("Expiration date");
    TextArea keyContent = new TextArea("Certificate key");
    TextArea certContent = new TextArea("Certificate");
    CertKeyUploader keyUploader = new CertKeyUploader();
    CertKeyUploader certUploader = new CertKeyUploader();


    public SSLCertificateForm(ServiceSSLCertContentService service) {
        super(service);
    }

    @Override
    protected Component createContent() {

        keyUploader.setField(keyContent);
        certUploader.setField(certContent);
        Upload keyUpload = new Upload("Key form file", keyUploader);
        keyUpload.addSucceededListener(keyUploader);
        Upload certUpload = new Upload("Certificate from file", certUploader);
        certUpload.addSucceededListener(certUploader);
        return new MVerticalLayout(
                new MFormLayout(
                        name,
                        expirationDate,
                        keyContent,
                        keyUpload,
                        certContent,
                        certUpload
                ).withMargin(true).withUndefinedSize(),
                getToolbar()
        );
    }

    class CertKeyUploader implements Upload.Receiver, Upload.SucceededListener {
        File file = null;
        AbstractTextField field;

        public void setField(AbstractTextField field) {
            this.field = field;
        }

        @Override
        public OutputStream receiveUpload(String filename, String mimeType) {
            try {
                String tempDir = System.getProperty("java.io.tmpdir");
                file = new File(tempDir+File.separator+filename);
                OutputStream os = new FileOutputStream(file);
                return os;
            } catch (IOException ex) {

            }
            return null;
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent event) {
            if (field == null) {
                return;
            }
            try (FileInputStream fs = new FileInputStream(file)) {
                byte[] buffer = new byte[fs.available()];
                fs.read(buffer);
                fs.close();
                file.delete();
                field.setValue(new String(buffer));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
}
