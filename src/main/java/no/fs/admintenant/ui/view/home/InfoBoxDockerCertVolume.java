package no.fs.admintenant.ui.view.home;

import com.github.appreciated.material.MaterialTheme;
import no.fs.admintenant.services.events.DockerServiceState;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.label.MLabel;

@Component
public class InfoBoxDockerCertVolume extends InfoBoxDocker {
    @Override
    public String getTitle() {
        return "Certificate volume";
    }

    @Override
    public DockerServiceState getDockerServiceState() {
        return DockerServiceState.CERT_VOLUME;
    }

    @Override
    public void buildLayout() {
        super.buildLayout();

        super.buildLayout();
        if (!dockerService.isProxyCertVolumePresent()) {
            add(
                    new MLabel("Volume not present!").withStyleName(MaterialTheme.LABEL_FAILURE)
            );
        } else {
            add(
                    new MLabel("Name", dockerService.getProxyCertVolume().getName()),
                    new MLabel("Driver",dockerService.getProxyCertVolume().getDriver())

            );
        }
    }
}
