package no.fs.admintenant.ui.view.services;

import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.ui.*;
import no.fs.admintenant.domain.model.ServiceInstance;
import no.fs.admintenant.domain.model.ServiceSSLCertContent;
import no.fs.admintenant.domain.model.ServiceTemplate;
import no.fs.admintenant.domain.services.ServiceInstanceService;
import no.fs.admintenant.domain.services.ServiceSSLCertContentService;
import no.fs.admintenant.domain.services.ServiceTemplateService;
import no.fs.admintenant.ui.data.EntityDataProvider;
import no.fs.admintenant.ui.forms.BaseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.viritin.fields.EnumSelect;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

public class ServiceForm extends BaseForm<ServiceInstance, ServiceInstanceService> {

    MTextField name = new MTextField("Name");
    MTextField domainName = new MTextField("Domain name");
    LabelField<ServiceTemplate> serviceTemplate = new LabelField<ServiceTemplate>("Template");
    LabelField<ServiceSSLCertContent> serviceSSLCertContent = new LabelField<>("Certificate");
    NativeSelect<ServiceTemplate> serviceTemplates;
    NativeSelect<ServiceSSLCertContent> serviceSSLCertContentListSelects;


    public ServiceForm(ServiceInstanceService service,ServiceTemplateService serviceInstanceService,ServiceSSLCertContentService serviceSSLCertContentService) {
        super(service);
        serviceTemplates = new NativeSelect<>("Available templates");
        serviceTemplates.setDataProvider(new EntityDataProvider<ServiceTemplate>(serviceInstanceService));
        serviceTemplates.setEmptySelectionAllowed(true);

        serviceTemplates.addValueChangeListener(new HasValue.ValueChangeListener<ServiceTemplate>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<ServiceTemplate> event) {
                serviceTemplate.setValue(event.getValue());
            }
        });
        serviceSSLCertContentListSelects = new NativeSelect<>("SSL certificates");
        serviceSSLCertContentListSelects.setDataProvider(new EntityDataProvider<ServiceSSLCertContent>(serviceSSLCertContentService));
        serviceSSLCertContentListSelects.setEmptySelectionAllowed(true);

        serviceSSLCertContentListSelects.addValueChangeListener(new HasValue.ValueChangeListener<ServiceSSLCertContent>() {
            @Override
            public void valueChange(HasValue.ValueChangeEvent<ServiceSSLCertContent> event) {
                serviceSSLCertContent.setValue(event.getValue());
            }
        });
    }

    @Override
    public void setBinder(Binder<ServiceInstance> binder) {
        super.setBinder(binder);

    }

    @Override
    protected Component createContent() {
        if(getEntity().getServiceTemplate()!=null) {
            serviceTemplates.setSelectedItem(getEntity().getServiceTemplate());
        }
        if(getEntity().getServiceSSLCertContent()!=null){
            serviceSSLCertContentListSelects.setSelectedItem(getEntity().getServiceSSLCertContent());
        }
        if(getEntity().getDeployed()){
            name.setReadOnly(true);
            serviceTemplate.setReadOnly(true);
            serviceTemplates.setReadOnly(true);
        }
        return new MVerticalLayout(
                new MFormLayout(
                        name,
                        domainName,
                        serviceTemplates,
                        serviceSSLCertContentListSelects

                ).withMargin(true).withUndefinedSize(),
                getToolbar());
    }

    public ServiceTemplate getServiceTemplate(){
        return serviceTemplates.getSelectedItem().orElse(null);
    }

    public ServiceSSLCertContent getSSL(){
        return serviceSSLCertContentListSelects.getSelectedItem().orElse(null);
    }


}
