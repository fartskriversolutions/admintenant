package no.fs.admintenant.ui.view.home;

import com.github.appreciated.app.layout.annotations.MenuCaption;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.github.appreciated.material.MaterialTheme;
import com.github.dockerjava.api.model.Info;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import no.fs.admintenant.services.DockerService;
import no.fs.admintenant.services.events.DockerServiceState;
import no.fs.admintenant.ui.components.InfoBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.Event;
import org.vaadin.spring.events.EventBusListener;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import java.util.EventListener;

@SpringComponent
@UIScope
@SpringView(name = HomeView.NAME)
@MenuCaption("Home")  // Set the Caption for the NavigationButton
@MenuIcon(VaadinIcons.HOME) // Set the Icon for the NavigationButton
public class HomeView extends VerticalLayout implements View{
    public final static String NAME = "";

    @Autowired
    DockerService dockerService;

    @Autowired
    InfoBoxDocker infoBoxDockerInfo;

    @Autowired
    InfoBoxDockerProxyNetwork infoBoxDockerProxyNetwork;

    @Autowired
    InfoBoxDockerCertVolume infoBoxDockerCertVolume;


    @PostConstruct
    void init() {
        dockerService.reload();

        MVerticalLayout mainLayout = new MVerticalLayout();

        MHorizontalLayout row1 = new MHorizontalLayout().withFullWidth();
        row1.add(infoBoxDockerInfo.withFullWidth());
        row1.add(infoBoxDockerProxyNetwork.withFullWidth());
        row1.add(infoBoxDockerCertVolume.withFullWidth());
        mainLayout.add(row1);

        addComponent(mainLayout);
    }


    @Override
    public void attach(){
        super.attach();
        if(dockerService!=null){
            dockerService.reload();
        }
    }
}
