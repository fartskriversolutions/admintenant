package no.fs.admintenant.ui.view.home;

import com.github.appreciated.material.MaterialTheme;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Info;
import com.vaadin.ui.UI;
import no.fs.admintenant.services.DockerService;
import no.fs.admintenant.services.events.DockerServiceState;
import no.fs.admintenant.ui.components.InfoBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.events.Event;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;
import org.vaadin.viritin.label.MLabel;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public abstract class InfoBoxDocker extends InfoBox implements EventBusListener<DockerServiceState> {

    @Autowired
    DockerService dockerService;

    @Autowired
    EventBus.ApplicationEventBus eventBus;

    public abstract String getTitle();

    public abstract DockerServiceState getDockerServiceState();

    public InfoBoxDocker() {
        super();
        this.setCaption(getTitle());
    }

    @PostConstruct
    void init(){
        eventBus.subscribe(this);
        buildLayout();
    }


    @Override
    public void onEvent(org.vaadin.spring.events.Event<DockerServiceState> event) {
        if (event.getPayload().equals(getDockerServiceState())) {
            UI.getCurrent().access(()->{
                buildLayout();
            });
        }
    }

    @Override
    public void buildLayout() {
        contentLayout.removeAllComponents();


    }

    @PreDestroy
    void destroy(){
        eventBus.unsubscribe(this);
    }
}
