package no.fs.admintenant.ui.view.home;

import com.github.appreciated.material.MaterialTheme;
import com.github.dockerjava.api.model.Info;
import no.fs.admintenant.services.events.DockerServiceState;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.label.MLabel;

@Component
public class InfoBoxDockerInfo extends InfoBoxDocker {
    @Override
    public String getTitle() {
        return "Docker info";
    }

    @Override
    public DockerServiceState getDockerServiceState() {
        return DockerServiceState.DOCKER_ENGINE;
    }

    @Override
    public void buildLayout() {
        super.buildLayout();
        if (dockerService.getDockerEngineIsPresent()) {
            Info info = dockerService.getDockerInfo();
            add(
                    new MLabel("Architecture", info.getArchitecture()),
                    new MLabel("OS Type", info.getOsType()),
                    new MLabel("Engine version", info.getServerVersion()),
                    new MLabel("KernelVersion", info.getKernelVersion()),
                    new MLabel("Memory total", info.getMemTotal().toString()),
                    new MLabel("CPUs", info.getNCPU().toString())
            );
        } else {
            add(
                    new MLabel("Docker service not present or running!").withStyleName(MaterialTheme.LABEL_FAILURE)
            );
        }
    }
}
