package no.fs.admintenant.ui.view.SSLSertivicates;

import com.github.appreciated.app.layout.annotations.MenuCaption;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import no.fs.admintenant.domain.model.ServiceSSLCertContent;
import no.fs.admintenant.domain.services.ServiceSSLCertContentService;
import no.fs.admintenant.services.DockerService;
import no.fs.admintenant.ui.components.BaseGrid;
import no.fs.admintenant.ui.components.Notifications;
import no.fs.admintenant.ui.core.IAdminView;
import no.fs.admintenant.ui.forms.BaseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.management.Notification;

@SpringView(name = SSLCertAdminView.NAME)
@MenuCaption(SSLCertAdminView.NAME)  // Set the Caption for the NavigationButton
@MenuIcon(VaadinIcons.ENVELOPE_O) // Set the Icon for the NavigationButton
public class SSLCertAdminView extends MVerticalLayout implements IAdminView<ServiceSSLCertContent,ServiceSSLCertContentService>{

    public final static String NAME = "Certificates";

    @Autowired
    ServiceSSLCertContentService serviceSSLCertContentService;

    @Autowired
    DockerService dockerService;

    SSLCertificateForm sslCertificateForm;

    BaseGrid<ServiceSSLCertContent,ServiceSSLCertContentService> certGrid;

    @Override
    public void reload() {
        certGrid.getDataProvider().refreshAll();
    }

    @PostConstruct
    void init() {
        addComponent(new Header("Certificates admin"));

        MHorizontalLayout _toolbox = new MHorizontalLayout();
        _toolbox.add(new MButton("Add new certificate", a -> newEntityForm()));
        _toolbox.add(new MButton("Deploy", a -> deployCertificate()));
        addComponent(_toolbox);

        certGrid = new BaseGrid<ServiceSSLCertContent,ServiceSSLCertContentService>(serviceSSLCertContentService);
        certGrid.withProperties(
                "name","expirationDate",
                "createdAt", "updatedAt").withFullWidth();
        certGrid.addEditColumn(e -> editEntity((ServiceSSLCertContent) e));
        certGrid.addDeleteColumn(e -> deleteEntity((ServiceSSLCertContent) e));
        certGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        add(certGrid);


    }

    @Override
    public ServiceSSLCertContentService getService() {
        return serviceSSLCertContentService;
    }

    @Override
    public BaseForm<ServiceSSLCertContent, ServiceSSLCertContentService> getEditForm() {
        if(sslCertificateForm != null){
            return sslCertificateForm;
        }

        sslCertificateForm = new SSLCertificateForm(serviceSSLCertContentService);
        //sslCertificateForm.setWidth(70,Unit.PERCENTAGE);
        sslCertificateForm.setSavedHandler(this::saveEntity);
        sslCertificateForm.setResetHandler(this::resetEntity);

        return sslCertificateForm;
    }

    public void deployCertificate(){
        if(certGrid.getSelectedItem()==null){
            Notifications.showError("Please select certificate to deploy.");
            return;
        }
        dockerService.updateCertificates(certGrid.getSelectedItem(),true);
    }
}
