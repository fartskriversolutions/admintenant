package no.fs.admintenant.ui.view.services.templates;

import com.github.appreciated.app.layout.annotations.MenuCaption;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import no.fs.admintenant.domain.model.ServiceTemplate;
import no.fs.admintenant.domain.services.ServiceInstanceService;
import no.fs.admintenant.domain.services.ServiceTemplateService;
import no.fs.admintenant.services.DockerService;
import no.fs.admintenant.ui.components.BaseGrid;
import no.fs.admintenant.ui.components.Notifications;
import no.fs.admintenant.ui.core.IAdminView;
import no.fs.admintenant.ui.forms.BaseForm;
import no.fs.admintenant.ui.view.services.ServiceUpdaterWindow;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

@SpringView(name=ServiceTemplateView.NAME)
@MenuCaption("Templates")
@MenuIcon(VaadinIcons.CODE)
public class ServiceTemplateView extends MVerticalLayout implements IAdminView<ServiceTemplate,ServiceTemplateService>{
    public final static String NAME = "ServiceTemplates";

    @Autowired
    ServiceTemplateService serviceTemplateService;

    @Autowired
    ServiceInstanceService serviceInstanceService;

    @Autowired
    DockerService dockerService;

    BaseGrid<ServiceTemplate,ServiceTemplateService> templatesGrid;

    ServiceTemplateForm templateEditForm;


    @PostConstruct
    void init(){
        addComponent(new Header("Service templates"));
        templatesGrid = getTemplatesGrid();
        templatesGrid.withFullWidth();

        addComponent(templatesGrid);
    }

    BaseGrid<ServiceTemplate,ServiceTemplateService> getTemplatesGrid(){
        if(templatesGrid!=null){
            return templatesGrid;
        }

        templatesGrid = new BaseGrid<ServiceTemplate, ServiceTemplateService>(serviceTemplateService);
        templatesGrid.withProperties(
                "name",
                "description",
                "createdAt", "updatedAt"
        ).withFullWidth();
        templatesGrid.addEditColumn(e -> editEntity(e));

        return templatesGrid;
    }

    @Override
    public void editEntity(ServiceTemplate entity) {
        dockerService.reload();

        if(!dockerService.getDockerEngineIsPresent()){
            Notifications.showError("Currently is not possible edit template if docker engine not running!");
            return;
        }

        String id = entity.getId();
        if (id != null) {
            MessageBox.createQuestion()
                    .withMessage("Change the template will restart all running services witch based on it!")
                    .withYesButton(()->{
                        try {
                            getEditForm().setEntity(getService().findOne(id));
                            Window popup = getEditForm().openInModalPopup();
                        } catch (Exception e) {
                            Notifications.showError(e.getMessage());
                        }

                    })
                    .withNoButton(ButtonOption.focus())
                    .open();


        }
    }

    @Override
    public void saveEntity(ServiceTemplate entity) {
        try {
            entity = getService().save(entity);

        }catch (Exception ex){
            Notifications.showError(ex.getMessage());
            return;
        }
        getEditForm().getPopup().close();
        ServiceUpdaterWindow serviceUpdaterWindow = new ServiceUpdaterWindow(dockerService);
        serviceUpdaterWindow.setServiceInstances(serviceInstanceService.getDeployedByTemplate(entity));
        serviceUpdaterWindow.start();
        reload();
    }

    @Override
    public void reload() {
        templatesGrid.getDataProvider().refreshAll();
    }

    @Override
    public ServiceTemplateService getService() {
        return serviceTemplateService;
    }

    @Override
    public BaseForm<ServiceTemplate, ServiceTemplateService> getEditForm() {
        if(templateEditForm!=null){
            return templateEditForm;
        }

        templateEditForm = new ServiceTemplateForm(serviceTemplateService);
        templateEditForm.setSavedHandler(this::saveEntity);
        templateEditForm.setResetHandler(this::resetEntity);
        return templateEditForm;
    }
}
