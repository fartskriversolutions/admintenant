docker network create proxy --driver overlay
docker volume create proxy_cert_volume 
docker service create \
--name nginx-proxy \
--publish 443:443 \
--publish 80:80 \
--mode global \
--constraint 'node.role == manager' \
--network proxy \
--mount type=bind,src=/var/run/docker.sock,dst=/tmp/docker.sock \
--mount type=bind,src=/Users/denz0x13/devel/mix/admintenant/certs,dst=/etc/nginx/certs \
jwilder/nginx-proxy 
