FROM anapsix/alpine-java:8

RUN apk add --no-cache bash curl git openssh-client

ENV DOCKER_VERSION "17.09.1-ce"
RUN curl -L -o /tmp/docker-$DOCKER_VERSION.tgz https://download.docker.com/linux/static/stable/x86_64/docker-$DOCKER_VERSION.tgz \
    && tar -xz -C /tmp -f /tmp/docker-$DOCKER_VERSION.tgz \
    && mv /tmp/docker/docker /usr/bin \
    && rm -rf /tmp/docker-$DOCKER_VERSION /tmp/docker

VOLUME /tmp
ADD build/libs/admintenant.jar app.jar
RUN bash -c 'touch /app.jar'
RUN bash -c 'mkdir /certs'
EXPOSE 8080


ENV DOCKER_HOST=unix:///var/run/docker.sock
#ENV JAVA_OPTS="-Dlogging.file=/tmp/logs/agritex-app-dev.log"
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
#ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]